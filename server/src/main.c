#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <dirent.h>
#include <gmp.h>
#include <ostara.h>
#include <nocrypt.h>
#include <signal.h>
#include <sys/stat.h>
#include <wat/file.h>
#include <wat/string.h>
#include <wat/tree.h>

#define FALLBACK_PRIME "a6bad72e7b1d69dfc4d379092c80361deb25d90ba501f05aa2b6c8b1593bafab" \
                       "8e7612994b2c857788b131a1aa267c1bd3fb8f46b6331722bc1e065ed529a7dc" \
                       "e57f168210b38078eeb8cde341b4d660b76bf713a7aa186cb315322e08b74ed5" \
                       "805a574b2a03e7b2962a3ac23b627424a08cd6ec32ea23c0baddb3fe2468ab2a" \
                       "7accba3a5b68dbb2cccf46f0a94358126cfb0ae5b0f20ce3bd746005339fd8b2" \
                       "b8491716455a42fedc577b44171d6fdab92a35e0cdd628820a2b846ec40be9ab" \
                       "20ca7daaf77d5aa2c578662a26e03d384aa21c4c6e3ff25ca6190f49360c4724" \
                       "69f624562f92f6aca10a7a6d75e6acf9c83549c242bc4a267f6b9dcfd19d32b7" \
                       "4b7e8d079bb9e2de002f8edb78e214a2578972feacb798a1de45e259c5714e82" \
                       "c542953d9c9a704ee1c4d6235da12a9c780b806a3a759750ce40eed3e21a8e46" \
                       "0975fcb0bec2bf7ee2593b12798825a2cd60ec4761652ea7e038de9875d66e0b" \
                       "9aca0800f5d03264f50ed5d5bac7cf21b7dcb71b9b8adb2ad94fae8aa720ccfb" \
                       "111e041358bb2dffd5d65ecd6fd43c9ca8b12a44ead55a668dcdea15087b3031" \
                       "faf5f10e8c6360467070887d32493169753a76488b15be0337041a7892e16f23" \
                       "fb8804def3da52a38a5a717abbdee5628c939dc0deb53c9070ad9718592eb636" \
                       "25bef07c9b8ea9ba01df39cc6c7c946a38c712c8122847f3edcb2d1db7f8f6eb"

// logging

#define print_info(...) print_msg(95, "info", __VA_ARGS__)
#define print_warning(...) print_msg(93, "warning", __VA_ARGS__)
#define print_error(...) print_msg(91, "error", __VA_ARGS__)
#define print_sys_error(e) print_error("%s\n", sys_errlist[e])
#define print_nc_error(e) print_error("%s\n", nc_error_str[e])
#define print_ost_error(e) print_error("%s\n", ost_error_str[e])

int print_msg(unsigned int pre_colour, char *pre, char *fmt, ...) {
    char _fmt[256];
    va_list ap;
    int n;

    snprintf(_fmt, sizeof(_fmt), "\e[%um%s\e[37m:\e[0m %s", pre_colour, pre, fmt);

    va_start(ap, fmt);
    n = vfprintf(stderr, _fmt, ap);
    va_end(ap);

    return n;
}

//
// config
//

char *config_path = "ostara.conf";

struct {
    int port,
        timeout;
    struct ost_limits limits;
    struct {
        ssize_t key_size,
                x_size;
        bool prime_autogen;
        size_t prime_autogen_size;
        char *prime_file;
        mpz_t prime_fallback;
    } keyx;
} config;

int strtobool(char *str, bool *result) {
    if(strcmp(str, "true") == 0 || strcmp(str, "yes") == 0) {
        *result = true;
        return 0;
    }
    else if(strcmp(str, "false") == 0 || strcmp(str, "no") == 0) {
        *result = false;
        return 0;
    }
    else {
        return -1;
    }
}

void init_config_defaults(void) {
    config.port = -1;
    config.timeout = -1;

    config.limits.buffer_size = -1;
    config.limits.data_quota = -1;

    config.keyx.key_size = 8; // 64 bits
    config.keyx.x_size = 16; // 128 bits
    config.keyx.prime_autogen = false;
    config.keyx.prime_autogen_size = 2048;
    config.keyx.prime_file = NULL;

    mpz_init(config.keyx.prime_fallback);
    mpz_set_str(config.keyx.prime_fallback, FALLBACK_PRIME, 16);
}

int load_config(void) {
    int e;
    FILE *fh;
    char *buf,
         *key,
         *val,
         *end;
    size_t len,
           i;
    struct strtoken_state s;

    fh = fopen(config_path, "r");
    if(fh == NULL) {
        print_info("no configuration found\n");
        return OST_OK;
    }

    if(freadall(&buf, NULL, fh) == -1) {
        e = OST_MALLOC;
        goto e0;
    }

    strtoken_init(&s, STRTOKEN_DUP);

    while(true) {
        e = strtoken(buf, -1, &s, "=", &key, NULL);
        if(e == -1) {
            e = OST_MALLOC;
            goto e0;
        }

        if(strtoken(buf, -1, &s, NULL, &val, NULL) == -1) {
            e = OST_MALLOC;
            goto e1;
        }

        if(e == 0) {
            if(strcmp(key, "port") == 0) {
                errno = 0;
                config.port = strtoull(val, &end, 10);
                if(errno != 0 || end == val)
                    print_error("invalid value for option: port\n");
            }
            else if(strcmp(key, "timeout") == 0) {
                errno = 0;
                config.timeout = strtoull(val, &end, 10);
                if(errno != 0 || end == val)
                    print_error("invalid value for option: port\n");
            }
            else if(strcmp(key, "keyx.prime_autogen") == 0) {
                if(strtobool(val, &config.keyx.prime_autogen) == -1)
                    print_error("invalid value for option: keyx.prime_autogen\n");
            }
            else if(strcmp(key, "keyx.prime_autogen_size") == 0) {
                errno = 0;
                config.keyx.prime_autogen_size = strtoull(val, &end, 10);
                if(errno != 0 || end == val)
                    print_error("invalid value for option: keyx.autogen_prime_size\n");
            }
            else if(strcmp(key, "keyx.prime_file") == 0) {
                config.keyx.prime_file = strdup(val);
                if(config.keyx.prime_file == NULL) {
                    e = OST_MALLOC;
                    goto e1;
                }
            }
            else if(strcmp(key, "keyx.prime_fallback") == 0) {
                if(mpz_set_str(config.keyx.prime_fallback, val, 10) != 0)
                    print_error("invalid value for option: keyx.prime_fallback\n");
            }

            free(key);
        }
        else if(e == 1) {
            // TODO: non-keyed options
        }

        free(val);
    }

    free(buf);

    return OST_OK;

e1: free(key);

e0: return e;
}

int autogen_dh_prime(struct ost_server *s) {
    int e;
    mpz_t p;

    e = nc_dh_gen_prime(config.keyx.prime_autogen_size/8, p);
    if(e != NC_OK)
        return -1;

    ost_server_configure_keyx(s, config.keyx.key_size, config.keyx.x_size, 2, p);

    return 0;
}

int save_dh_prime(struct ost_server *s) {
    int e;
    char *ascii;
    size_t ascii_len,
           nw;
    char path[PATH_MAX];
    FILE *fh;

    e = nc_dh_prime_to_ascii(s->secure.keyx.p, &ascii, &ascii_len);
    if(e != NC_OK)
        return 1;

    snprintf(path, sizeof(path), "keyx-prime-%zu.nc", config.keyx.prime_autogen_size);

    fh = fopen(path, "w+");
    if(fh == NULL)
        return 1;

    nw = fwrite(ascii, 1, ascii_len, fh);
    fclose(fh);

    if(nw < ascii_len)
        return 1;

    return 0;
}

//
// event stream test (continuous response)
//

#define EVENT_INTERVAL 3
int ecid;
ptime_t last_event;

int serve_event(struct ost_conn *c, void *aux) {
    int ctx_id;
    size_t i;
    ptime_t interval,
            now;
    char id[17],
         data[256];
    size_t len;

    now = time_now(CLOCK_REALTIME);
    if(now-last_event >= EVENT_INTERVAL) { // send an event every 2.5 seconds
        strrandlex(id, sizeof(id), "abcdefghijklmnopqrstuvwxyz", -1);
        len = snprintf(data, sizeof(data), "{id:'%s', time:%.03Lf}\n", id, now);
        ost_str_data(c, ecid, "body", data, len);
        last_event = now;
    }

    return OST_OK;
}

ost_handler(r_events) {
    srand(time(NULL));

    ecid = ctx_id;
    last_event = time_now(CLOCK_REALTIME);

    ost_str_data(c, ctx_id, "transfer-type", "continuous", -1);
    ost_status(c, ctx_id, OST_RSP_CONTINUE);
    ost_commit(c, ctx_id, NULL);

    ost_conn_set_timeout(c, -1);
    ost_conn_set_worker_out(c, serve_event, NULL);
}

//
// file and directory serving
//

int qsort_dir_dynamic(const struct tree_node **ap, const struct tree_node **bp) {
    const struct tree_node *a,
                           *b;
    bool is_dir[2];
    char *type[2],
         *name[2];

    a = *ap;
    b = *bp;

    if(tree_get((struct tree_node *)a, "type", TREE_NODE_STR, &type[0]) == TREE_OK &&
       tree_get((struct tree_node *)b, "type", TREE_NODE_STR, &type[1]) == TREE_OK)
    {
        is_dir[0] = strcmp(type[0], "directory") == 0;
        is_dir[1] = strcmp(type[1], "directory") == 0;
    }
    else {
        goto s2; // try to sort typeless entries by name
    }

    // directories first
    if(is_dir[0] && is_dir[1] == false) {
        return -1;
    }
    else if(is_dir[0] == false && is_dir[1]) {
        return +1;
    }
    else {
    s2: if(tree_get((struct tree_node *)a, "name", TREE_NODE_STR, &name[0]) == TREE_OK &&
           tree_get((struct tree_node *)b, "name", TREE_NODE_STR, &name[1]) == TREE_OK)
        {
            // non-hidden names first
            if(name[0][0] != '.' && name[1][0] == '.')
                return -1;
            else if(name[0][0] == '.' && name[1][0] != '.')
                return +1;
            // fall back to lexographical sorting
            else
                return strcmp(name[0], name[1]);
        }
        else {
            return +1; // nameless entries go last
        }
    }
}

double ts_to_double(struct timespec *ts) {
    return (double)ts->tv_sec + ((double)ts->tv_nsec/1e9);
}

void serve_directory(struct ost_conn *c, int ctx, char *dir_path) {
    int e;
    unsigned int rsp;
    DIR *dp;
    struct tree_node *r,
                     *n;
    char path[PATH_MAX],
         *type,
         *str;
    size_t len,
           i;
    struct dirent *ep;
    struct stat sbuf;
    long double last_accessed,
                last_modified;

    dp = opendir(dir_path);
    if(dp == NULL) {
        rsp = OST_RSP_ERROR;
        goto r1;
    }

    e = tree_node_new(&r, TREE_NODE_LIST, NULL);
    if(e != TREE_OK) {
        rsp = OST_RSP_ERROR;
        goto r2;
    }

    // add entry for parent working directory
    if(strcmp(dir_path, ".") != 0) {
        len = snprintf(path, PATH_MAX, "%s", dir_path);
        for(i=len-1; i > 0; --i) {
            if(path[i] == '/') {
                path[i] = '\0';
                break;
            }
        }

        last_accessed = ts_to_double(&sbuf.st_atim);
        last_modified = ts_to_double(&sbuf.st_mtim);

        if(tree_set(r, "[-1:].name", TREE_NODE_STR, ep->d_name) != TREE_OK ||
           tree_set(r, "[-1].size", TREE_NODE_INT, &sbuf.st_size) != TREE_OK ||
           tree_set(r, "[-1].type", TREE_NODE_STR, "directory") != TREE_OK ||
           tree_set(r, "[-1].last_accessed", TREE_NODE_FLOAT, &last_accessed) != TREE_OK ||
           tree_set(r, "[-1].last_modified", TREE_NODE_FLOAT, &last_modified) != TREE_OK)
        {
            rsp = OST_RSP_ERROR;
            goto r2;
        }
    }

    while(true) {
        ep = readdir(dp);
        if(ep == NULL)
            break;

        // skip dot-prefixed crap
        if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0)
            continue;

        snprintf(path, PATH_MAX, "%s/%s", dir_path, ep->d_name);

        if(stat(path, &sbuf) == 0 && S_ISDIR(sbuf.st_mode))
            type = "directory";
        else
            type = "file";

        last_accessed = ts_to_double(&sbuf.st_atim);
        last_modified = ts_to_double(&sbuf.st_mtim);

        if(tree_set(r, "[-1:].name", TREE_NODE_STR, ep->d_name) != TREE_OK ||
           tree_set(r, "[-1].size", TREE_NODE_INT, &sbuf.st_size) != TREE_OK ||
           tree_set(r, "[-1].type", TREE_NODE_STR, type) != TREE_OK ||
           tree_set(r, "[-1].last_accessed", TREE_NODE_FLOAT, &last_accessed) != TREE_OK ||
           tree_set(r, "[-1].last_modified", TREE_NODE_FLOAT, &last_modified) != TREE_OK)
        {
            rsp = OST_RSP_ERROR;
            goto r2;
        }
    }

    tree_qsort(r, qsort_dir_dynamic);

    e = tree_serialize(r, true, "  ", &str, &len);
    if(e != TREE_OK) {
        rsp = OST_RSP_ERROR;
        goto r2;
    }

    ost_str_data(c, ctx, "body", str, len);
    rsp = OST_RSP_OK;

r2: closedir(dp);
r1: if(r != NULL)
        tree_node_destroy(r);

    ost_status(c, ctx, rsp);
    ost_commit(c, ctx, NULL);
}

void serve_file(struct ost_conn *c, int ctx, char *path) {
    int e;
    unsigned int rsp;
    unsigned long int offset;
    long int len;
    FILE *fh;

    if(ost_get_uint_data(c, ctx, "content-offset", &offset) != OST_OK)
        offset = 0;
    if(ost_get_int_data(c, ctx, "content-length", &len) != OST_OK)
        len = -1;

    ost_debug("content-offset=\e[97m%lu\e[0m\n", offset);
    ost_debug("content-length=\e[97m%li\e[0m\n", len);

    e = ost_conn_set_stream_out(c, ctx, path, offset, len, c->limits.buffer_size);
    if(e == OST_BAD_INPUT) {
        rsp = OST_RSP_BAD_INPUT;
        goto r2;
    }
    else if(e != OST_OK) {
        rsp = OST_RSP_ERROR;
        goto r2;
    }

    ost_str_data(c, ctx, "transfer-type", "download", -1);
    ost_str_data(c, ctx, "content-disposition", "attachment", -1);
    if(offset > 0)
        ost_uint_data(c, ctx, "content-offset", offset);
    if(len >= 0)
        ost_int_data(c, ctx, "content-length", len);

    rsp = OST_RSP_CONTINUE;

r2: free(path);
r1: ost_status(c, ctx, rsp);
    ost_commit(c, ctx, NULL);
}

ost_handler(r_serve) {
    int e,
        rsp;
    char *path;
    struct stat sbuf;

    e = ost_get_token(q, "path", &path);
    if(e != OST_OK) {
        rsp = OST_RSP_BAD_INPUT;
        goto r1;
    }

    if(fsanitizepath(path, &path) != 0) {
        rsp = OST_RSP_ERROR;
        goto r1;
    }

    // subdirectory or file
    if(stat(path, &sbuf) == 0) {
        if(S_ISDIR(sbuf.st_mode))
            serve_directory(c, ctx_id, path);
        else
            serve_file(c, ctx_id, path);
    }
    // error
    else {
        rsp = OST_RSP_NOT_FOUND;
    r1: ost_status(c, ctx_id, rsp);
        ost_commit(c, ctx_id, NULL);
    }
}

//
// redirection test
//

ost_handler(r_redir) {
    ost_str_data(c, ctx_id, "body", "redirecting to file listing...", -1);
    ost_str_data(c, ctx_id, "location", "/files", -1);
    ost_status(c, ctx_id, OST_RSP_REDIRECT);
    ost_commit(c, ctx_id, NULL);
}

//
// main stuff
//

struct ost_server s;

void quit(int signal) {
    fwrite("\b\b", 2, 1, stdout);
    fflush(stdout);

    ost_server_stop(&s);
    print_info("server stopped\n");
}

int main(int argc, char **argv) {
    int e;
    struct sigaction sa;
    struct ost_limits limits;
    mpz_t p;

    // SIGINT handler
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = quit;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);

    init_config_defaults();
    if(load_config() == -1)
        return 1;

    e = ost_server_init(&s, config.port, config.timeout, &config.limits);
    if(e != OST_OK)
        return 1;

    if(config.keyx.prime_autogen) {
        if(autogen_dh_prime(&s) == -1)
            return 1;

        if(save_dh_prime(&s) == -1)
            return 1;
    }
    else if(config.keyx.prime_file != NULL) {
        e = nc_dh_prime_load(&p, config.keyx.prime_file);
        if(e != NC_OK)
            return 1;

        ost_server_configure_keyx(&s, config.keyx.key_size, config.keyx.x_size, 2, p);
    }
    else {
        ost_server_configure_keyx(&s, config.keyx.key_size, config.keyx.x_size, 2, config.keyx.prime_fallback);
    }

    ost_server_add_route(&s, "event-stream", "/events", false, r_events);
    ost_server_add_route(&s, "file-serve", "/files/$path", false, r_serve);
    ost_server_add_route(&s, "redir-test", "/redir", false, r_redir);

    e = ost_server_main(&s);
    if(e != OST_OK)
        fprintf(stderr, "\e[91merror:\e[0m %s\n", ost_error_str[e]);

    ost_server_deinit(&s);

    return 0;
}
