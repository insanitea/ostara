#ifndef OSTARA_H
#define OSTARA_H

#include <nocrypt.h>
#include <wat/time.h>

#include <netdb.h>     // getaddrinfo(), getnameinfo(), NI_MAXHOST
#include <sys/epoll.h>
#include <sys/types.h>

#define OST_KB(n) (n*1024)
#define OST_MB(n) (n*1024*1024)

#ifdef DEBUG
    #define ost_debug(...) fprintf(stderr, __VA_ARGS__)
#else
    #define ost_debug(...)
#endif

enum ost_error_code {
    OST_OK,
    OST_COMMIT,
    OST_REITER,
    OST_STOP,

    // * = non-critical

    OST_INTERRUPT,
    OST_SYSTEM,
    OST_MALLOC,

    OST_CONNECT,
    OST_HANG_UP,
    OST_TIMEOUT,
    OST_POLL,
    OST_ACCEPT,
    OST_SEND,
    OST_RECV,
    OST_BLOCK,
    OST_OPEN,
    OST_WRITE,
    OST_READ,
    OST_CHOKE,
    OST_PARTIAL,      // *

    OST_DATA_QUOTA,
    OST_PAYLOAD_SIZE,

    OST_KEYX,
    OST_BAD_QUERY,    // *
    OST_BAD_CTX,
    OST_BAD_CMD,
    OST_BAD_INPUT,
    OST_BAD_PAYLOAD,

    OST_ERROR,
    OST_BAD_URL,

    OST_ERROR_MAX
};

enum ost_rsp_code {
    // success
    OST_RSP_OK = 10,
    OST_RSP_CONTINUE,
    OST_RSP_REDIRECT,

    // client
    OST_RSP_NOT_FOUND = 20,
    OST_RSP_BAD_INPUT,
    OST_RSP_DATA_QUOTA,
    OST_RSP_UNAUTHORIZED,
    OST_RSP_FORBIDDEN,

    // server
    OST_RSP_ERROR = 30,
    OST_RSP_BAD_VERSION,
    OST_RSP_BAD_GATEWAY,
    OST_RSP_GATEWAY_TIMEOUT,
};

extern char *ost_error_str[];
extern char *ost_status_str[];

struct ost_conn;
struct ost_query_data;

typedef int (ost_conn_worker_in_cb)(struct ost_conn *c, int ctx_id, char *data, size_t len, void *aux);
typedef int (ost_conn_worker_out_cb)(struct ost_conn *c, void *aux);
typedef void (ost_handler_cb)(struct ost_conn *c, int ctx_id, struct ost_query_data *q, void *aux);

#define ost_handler(name) void name(struct ost_conn *c, int ctx_id, struct ost_query_data *q, void *aux)

struct ost_buf {
    char *head,
         *tail;
    size_t len,
           rem,
           size;
};

struct ost_route {
    char *tag,
         *tpl,
         **tplv;
    size_t tplc;
    bool temp;
    ost_handler_cb *callback;
    void *aux;
    struct ost_server *server;
    struct ost_route *prev,
                     *next;
};

struct ost_query_arg {
    char *key,
         *value;
    struct ost_query_arg *next;
};

struct ost_query_data {
    char *path,
         **pathv;
    size_t pathc;
    struct ost_query_arg *args,
                         *tokens;
    struct ost_route *route;
};

struct ost_limits {
    ssize_t buffer_size,
            data_quota;
};

struct ost_server {
    unsigned int port;
    struct sockaddr_storage addr;
    socklen_t addr_len;
    char host[NI_MAXHOST];
    volatile bool active;
    int epoll_fd,
        timeout;

    struct ost_limits limits;
    struct ost_conn *connections,
                    *connections_last;
    struct ost_route *routes,
                     *routes_last;

    struct {
        int state;
        struct nc_key key;
        struct {
            unsigned long int key_size,
                              x_size,
                              g;
            mpz_t p;
        } keyx;
    } secure;
};

struct ost_conn {
    struct ost_server *server;
    int fd,
        timeout;
    unsigned int port;
    struct sockaddr_storage addr;
    socklen_t addr_len;
    char host[NI_MAXHOST];
    struct ost_limits limits;

    void *contexts;
    size_t n_contexts,
           n_contexts_alloc;
    bool data_in_fill;
    void *data_in_ctx,
         *data_in_dest;
    size_t data_total;
    void *workers_out,
         *workers_out_last;
    int (*iter_in_func)(struct ost_conn *c);
    int (*iter_out_func)(struct ost_conn *c);
    volatile bool sync_in;
    volatile bool flush_out;
    ptime_t last_active;

    // buffers
    struct ost_buf main_in,
                   main_out,
                   data_in,
                   aux_in,  // decryption
                   aux_out; // encryption

    struct {
        int state,
            prev_state;

        struct {
            bool slave;
            volatile int in_progress; // 1=initial, 2=rekey
            unsigned long int peer_buf_size,
                              key_size,
                              x_size,
                              g;
            mpz_t p,x,X,Y;
        } keyx;

        struct nc_key key,
                      tmp_key;

        size_t max_payload_size;
        bool payload_in_fill,
             payload_in_avail;
    } secure;

    struct ost_conn *prev,
                    *next;
};

void ost_query_args_destroy(struct ost_query_arg *args);

int ost_buf_configure(struct ost_buf *buf, size_t max);
int ost_buf_realloc(struct ost_buf *buf, size_t max);
int ost_buf_grow(struct ost_buf *buf, size_t n);
void ost_buf_clear(struct ost_buf *buf);
void ost_buf_dealloc(struct ost_buf *buf);
void ost_buf_ltrim(struct ost_buf *buf, size_t n);
void ost_buf_rtrim(struct ost_buf *buf, size_t n);
int ost_buf_send(struct ost_buf *buf, struct ost_conn *c, ssize_t n);
int ost_buf_recv(struct ost_buf *buf, struct ost_conn *c, ssize_t n);
int ost_buf_fwrite(struct ost_buf *buf, FILE *fh, ssize_t n);
int ost_buf_fread(struct ost_buf *buf, FILE *fh, ssize_t n);
size_t ost_buf_fill(struct ost_buf *dest, struct ost_buf *src, ssize_t n);
int ost_buf_append(struct ost_buf *buf, char *data, ssize_t n);
void ost_buf_drop(struct ost_buf *buf, char **data_ret, size_t *len_ret);

int ost_server_init(struct ost_server *s, int port, int timeout, struct ost_limits *limits);
void ost_server_deinit(struct ost_server *s);
void ost_server_set_limits(struct ost_server *s, struct ost_limits *limits);
void ost_server_configure_keyx(struct ost_server *s, size_t key_size, size_t x_size, uint8_t g, mpz_t p);
int ost_server_configure_key(struct ost_server *s, struct nc_key *k);
void ost_server_attach_conn(struct ost_server *s, struct ost_conn *c);
void ost_server_detach_conn(struct ost_server *s, struct ost_conn *c);
int ost_server_add_route(struct ost_server *s, char *tag, char *tpl, bool temp, ost_handler_cb *cb);
int ost_server_rm_route(struct ost_server *s, char *tag, char *tpl);
int ost_server_process_query(struct ost_server *s, char *query, struct ost_query_data **qdata_ret);
int ost_server_main(struct ost_server *s);
void ost_server_stop(struct ost_server *s);

int ost_conn_new(int fd, unsigned int port, int timeout, struct ost_limits *limits,
                 struct sockaddr_storage *addr, socklen_t addr_len, struct ost_conn **conn_ret);
void ost_conn_close(struct ost_conn *c);
void ost_conn_destroy(struct ost_conn *c);
void ost_conn_configure_keyx(struct ost_conn *c, size_t key_size, size_t x_size, uint8_t g, mpz_t p);
int ost_conn_configure_key(struct ost_conn *c, struct nc_key *k);
void ost_conn_set_secure(struct ost_conn *c, bool secure);
void ost_conn_set_limits(struct ost_conn *c, struct ost_limits *limits);
void ost_conn_set_timeout(struct ost_conn *c, int timeout);
int ost_conn_set_worker_in(struct ost_conn *c, int ctx_id, char *key, ost_conn_worker_in_cb *callback, void *arg);
int ost_conn_set_worker_out(struct ost_conn *c, ost_conn_worker_out_cb *callback, void *arg);
int ost_conn_set_stream_in(struct ost_conn *c, int ctx_id, char *file_path, off_t offset, ssize_t len);
int ost_conn_set_stream_out(struct ost_conn *c, int ctx_id, char *file_path, off_t offset, ssize_t len, ssize_t chunk_size);

int ost_url_encode(char *str, char safe, char **str_ret);
int ost_url_decode(char *str, char **str_ret);
int ost_parse_url(char *url, char **host_ret, int *port_ret, char **query_ret);
int ost_parse_query(char *query, char **path_ret, struct ost_query_arg **args_ret);

int ost_connect(char *host, int port, int timeout, bool secure, struct ost_limits *limits, struct ost_conn **conn_ret);
int ost_queue(struct ost_conn *c, void *buf, size_t n);
int ost_flush(struct ost_conn *c);
int ost_sync(struct ost_conn *c);
void ost_interrupt(struct ost_conn *c);
int ost_data(struct ost_conn *c, int ctx_id, char *key, void *data, size_t size);
int ost_str_data(struct ost_conn *c, int ctx_id, char *key, char *str, ssize_t len);
int ost_int_data(struct ost_conn *c, int ctx_id, char *key, long int val);
int ost_uint_data(struct ost_conn *c, int ctx_id, char *key, unsigned long int val);
int ost_float_data(struct ost_conn *c, int ctx_id, char *key, long double val);
int ost_status(struct ost_conn *c, int ctx_id, unsigned int code);
int ost_commit(struct ost_conn *c, int ctx_id, char *query);

int ost_get_status(struct ost_conn *c, int ctx_id);
int ost_get_data(struct ost_conn *c, int ctx_id, char *key, void **data_ret, size_t *size_ret);
int ost_get_str_data(struct ost_conn *c, int ctx_id, char *key, char **str_ret, size_t *size_ret);
int ost_get_int_data(struct ost_conn *c, int ctx_id, char *key, long int *val_ret);
int ost_get_uint_data(struct ost_conn *c, int ctx_id, char *key, unsigned long int *val_ret);
int ost_get_float_data(struct ost_conn *c, int ctx_id, char *key, long double *val_ret);
int ost_get_arg(struct ost_query_data *q, char *key, char **arg_ret);
int ost_get_token(struct ost_query_data *q, char *key, char **tk_ret);

#endif
