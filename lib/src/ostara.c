#include "ostara.h"

#include <wat/string.h>
#include <wat/utf8.h>

#include <ctype.h>      // isspace()
#include <errno.h>
#include <fcntl.h>      // setsockopt()
#include <poll.h>       // poll()
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h> // socket(), bind(), listen(), accept(), connect()

#define max(a,b) ((a) > (b) ? (a) : (b))
#define mpz_byte_size(x) ((mpz_sizeinbase(x,2)+7)/8)

#define EPOLL_EVENT_MASK EPOLLHUP|EPOLLRDHUP|EPOLLIN
#define EPOLL_MAX_EVENTS 512
#define LISTEN_BACKLOG 64
#define KEYX_CTX -1

#define BUF_DEBUG_ROW_LEN 16
#define BUF_DEBUG_BYTE_COLOR "37"
#define BUF_DEBUG_CHAR_COLOR "96;40"
#define BUF_DEBUG_BASE_COLOR "40"

#define DEFAULT_PORT 2206

#define MIN_BUFFER_SIZE 128
#define MIN_DATA_QUOTA 128

#define DEFAULT_SERVER_BUFFER_SIZE OST_KB(4)
#define DEFAULT_SERVER_DATA_QUOTA OST_KB(8)

#define DEFAULT_CLIENT_BUFFER_SIZE OST_KB(4)
#define DEFAULT_CLIENT_DATA_QUOTA OST_MB(16)

//
// default limit structs
//

struct ost_limits default_server_limits = {
    DEFAULT_SERVER_BUFFER_SIZE,
    DEFAULT_SERVER_DATA_QUOTA
} ;

struct ost_limits default_client_limits = {
    DEFAULT_CLIENT_BUFFER_SIZE,
    DEFAULT_CLIENT_DATA_QUOTA
};

//
// key exchange state
//

enum ost_keyx_state {
    OST_SECURE_KEYX_NULL,
    OST_SECURE_KEYX_READY,
    OST_SECURE_KEYX_MAIN,
    OST_SECURE_ACTIVE
};

//
// error/status strings
//

char *ost_error_str[] = {
    [OST_OK] = "everything is okay",
    [OST_COMMIT] = "commit encountered",
    [OST_REITER] = "reiteration occurred",
    [OST_STOP] = "worker stopped",

    // general

    [OST_INTERRUPT] = "interrupted",
    [OST_SYSTEM] = "system error",
    [OST_MALLOC] = "out of memory",

    // connections and buffers

    [OST_CONNECT] = "cannot connect",
    [OST_HANG_UP] = "connection lost",
    [OST_TIMEOUT] = "timeout",
    [OST_POLL] = "poll/epoll error",
    [OST_ACCEPT] = "accept error",
    [OST_SEND] = "send error",
    [OST_RECV] = "recv error",
    [OST_BLOCK] = "send/recv blocked",
    [OST_OPEN] = "cannot open file",
    [OST_WRITE] = "write error",
    [OST_READ] = "read error",
    [OST_CHOKE] = "buffer choked",
    [OST_PARTIAL] = "nothing to process",

    // limits

    [OST_DATA_QUOTA] = "data quota exceeded",
    [OST_PAYLOAD_SIZE] = "maximum payload size exceeded",

    // command processing

    [OST_KEYX] = "key exchange failed",
    [OST_BAD_QUERY] = "bad path",
    [OST_BAD_CTX] = "bad context",
    [OST_BAD_CMD] = "bad command",
    [OST_BAD_INPUT] = "bad input",
    [OST_BAD_PAYLOAD] = "bad payload",

    // misc

    [OST_ERROR] = "something went wrong",
    [OST_BAD_URL] = "bad url",
};

char *ost_status_str[] = {
    [OST_RSP_OK] = "OK",
    [OST_RSP_CONTINUE] = "CONTINUE",
    [OST_RSP_REDIRECT] = "REDIRECT",

    [OST_RSP_NOT_FOUND] = "NOT FOUND",
    [OST_RSP_BAD_INPUT] = "BAD INPUT",
    [OST_RSP_DATA_QUOTA] = "DATA QUOTA",
    [OST_RSP_UNAUTHORIZED] = "UNAUTHORIZED",
    [OST_RSP_FORBIDDEN] = "FORBIDDEN",

    [OST_RSP_ERROR] = "INTERNAL ERROR",
    [OST_RSP_BAD_VERSION] = "BAD VERSION",
    [OST_RSP_BAD_GATEWAY] = "BAD GATEWAY",
    [OST_RSP_GATEWAY_TIMEOUT] = "GATEWAY TIMEOUT"
};

//
// internal debug helpers
//

inline static
void _ost_buf_debug_row(char *row, size_t n) {
    size_t i;

    fprintf(stderr, "\e[%sm", BUF_DEBUG_BYTE_COLOR);
    for(i=0; i < n; ++i)
        fprintf(stderr, "%02hhx", row[i]);
    fprintf(stderr, "\e[0m\n");

    for(i=0; i < n; ++i) {
        if(isprint(row[i])) {
            fprintf(stderr, "\e[%sm%c\e[0m\e[%sm \e[0m", BUF_DEBUG_CHAR_COLOR,
                                                         row[i],
                                                         BUF_DEBUG_BASE_COLOR);
        }
        else if(row[i] == '\n') {
            fprintf(stderr, "\e[%sm\\n\e[0m", BUF_DEBUG_CHAR_COLOR);
        }
        else {
            fprintf(stderr, "\e[%sm  \e[0m", BUF_DEBUG_BASE_COLOR);
        }
    }
    fprintf(stderr, "\n");
}

void ost_buf_debug(struct ost_buf *buf) {
    size_t max_contig,
           offset,
           rem;

    max_contig = (buf->len/BUF_DEBUG_ROW_LEN)*BUF_DEBUG_ROW_LEN;
    for(offset=0; offset < max_contig; offset+=BUF_DEBUG_ROW_LEN)
        _ost_buf_debug_row(buf->head+offset, BUF_DEBUG_ROW_LEN);

    rem = buf->len%BUF_DEBUG_ROW_LEN;
    if(rem > 0)
        _ost_buf_debug_row(buf->head+offset, rem);
}

//
// routes
//

inline static
void route_attach(struct ost_route *r, struct ost_server *s) {
    if(s->routes_last != NULL) {
        s->routes_last->next = r;
        r->prev = s->routes_last;
        s->routes_last = r;
    }
    else {
        s->routes = r;
        s->routes_last = r;
    }

    r->server = s;
}

inline static
void route_detach(struct ost_route *r) {
    struct ost_server *s;

    s = r->server;
    if(r == s->routes)
        s->routes = r->next;
    if(r == s->routes_last)
        s->routes_last = r->prev;

    if(r->prev != NULL)
        r->prev->next = r->next;
    if(r->next != NULL)
        r->next->prev = r->prev;

    r->server = NULL;
    r->prev = NULL;
    r->next = NULL;
}

inline static
void route_destroy(struct ost_route *r) {
    if(r->server != NULL)
        route_detach(r);

    free(r->tpl);
    strbuf_free(r->tplv, r->tplc);
    free(r);
}

//
// query data
//

void ost_query_args_destroy(struct ost_query_arg *args) {
    struct ost_query_arg *x,
                         *xn;

    for(x=args; x != NULL; x=xn) {
        xn = x->next;
        if(x->key != NULL)
            free(x->key);
        free(x->value);
        free(x);
    }
}

inline static
struct ost_query_arg * query_arg_new(char *key, char *value) {
    struct ost_query_arg *x;

    x = malloc(sizeof(struct ost_query_arg));
    if(x == NULL)
        return NULL;

    x->key = strdup(key);
    x->value = value;
    x->next = NULL;

    return x;
}

inline static
int query_arg_get(struct ost_query_arg *args, char *key, char **value_ret) {
    struct ost_query_arg *x;

    for(x=args; x != NULL; x=x->next) {
        if(strcmp(x->key, key) == 0) {
            if(value_ret != NULL)
                *value_ret = x->value;

            return OST_OK;
        }
    }

    return OST_ERROR;
}

inline static
struct ost_query_data * query_data_new(char *query) {
    int e;
    struct ost_query_data *q;
    char *path_stripped;

    q = malloc(sizeof(struct ost_query_data));
    if(q == NULL)
        goto e0;

    memset(q, 0, sizeof(struct ost_query_data));

    if(query != NULL) {
        e = ost_parse_query(query, &q->path, &q->args);
        if(e != OST_OK)
            goto e1;

        path_stripped = strstrip(q->path, -1, "/", true, NULL);
        if(path_stripped == NULL) {
            e = OST_MALLOC;
            goto e2;
        }

        q->pathv = strsplit(path_stripped, "/", &q->pathc);
        if(q->pathv == NULL)
            goto e3;

        free(path_stripped);
    }

    return q;

e3: free(path_stripped);
e2: free(q->path);
e1: free(q);

e0: return NULL;
}

inline static
void query_data_destroy(struct ost_query_data *q) {
    free(q->path);
    strbuf_free(q->pathv, q->pathc);
    ost_query_args_destroy(q->args);
    ost_query_args_destroy(q->tokens);
    free(q);
}

//
// data and connection state
//

struct conn_data {
    char *key,
         *buf;
    size_t size;
    ost_conn_worker_in_cb *callback;
    void *aux;
    struct conn_data *prev,
                     *next;
};

struct conn_ctx {
    int id,
        status;
    struct conn_data *data,
                     *data_last;
    volatile size_t pending_rsp_count;
};

inline static
bool conn_rsp_pending(struct ost_conn *c) {
    size_t i;
    struct conn_ctx *ctx;

    for(i=0; i < c->n_contexts; ++i) {
        ctx = &((struct conn_ctx *)c->contexts)[i];
        if(ctx->pending_rsp_count > 0)
            return true;
    }

    return false;
}

inline static
int conn_data_ensure(struct ost_conn *c, struct conn_ctx *ctx, char *key, struct conn_data **data_ret) {
    int e;
    struct conn_data *d,
                     *last;
    size_t key_len,
           new_data_total;

    for(d=ctx->data; d != NULL; d=d->next) {
        if(strcmp(d->key, key) == 0)
            break;
    }

    if(d == NULL) {
        key_len = strlen(key);

        // check data quota

        new_data_total = c->data_total+sizeof(struct conn_data)+key_len+1;
        if(new_data_total > c->limits.data_quota) {
            e = OST_DATA_QUOTA;
            goto e0;
        }

        // create data

        d = malloc(sizeof(struct conn_data));
        if(d == NULL) {
            e = OST_MALLOC;
            goto e0;
        }

        memset(d, 0, sizeof(struct conn_data));

        d->key = strdup(key);
        if(d->key == NULL) {
            e = OST_MALLOC;
            goto e1;
        }

        if(ctx->data_last != NULL) {
            last = ctx->data_last;
            last->next = d;
            d->prev = ctx->data_last;
            ctx->data_last = d;
        }
        else {
            ctx->data = d;
            ctx->data_last = d;
        }

        c->data_total = new_data_total;
    }
    else {
        // clear data

        if(d->buf != NULL) {
            free(d->buf);
            d->buf = NULL;
        }

        c->data_total -= d->size;
        d->size = 0;
    }

    if(data_ret != NULL)
        *data_ret = d;

    return OST_OK;

e1: free(d);

e0: return e;
}

inline static
int conn_data_prep_fill(struct ost_conn *c, struct conn_ctx *ctx, char *key, size_t size) {
    int e;
    struct conn_data *data;
    size_t new_data_total;
#ifdef DEBUG
    unsigned int qvc;
#endif

    e = conn_data_ensure(c, ctx, key, &data);
#ifdef DEBUG
    new_data_total = c->data_total+size;
    qvc = new_data_total > c->limits.data_quota ? 91 : 97;
    ost_debug("[%i: data in] quota = \e[%um%zu\e[0m/\e[%um%zu\e[0m\n", ctx->id,
              qvc, new_data_total, qvc, c->limits.data_quota);

    if(e != OST_OK)
        return e;
#else
    if(e != OST_OK)
        return e;

    new_data_total = c->data_total+size;
#endif

    if(new_data_total > c->limits.data_quota)
        return OST_DATA_QUOTA;

    e = ost_buf_configure(&c->data_in, size);
    if(e != OST_OK)
        return e;

    c->data_in_fill = true;
    c->data_in_ctx = ctx;
    c->data_in_dest = data;
    c->data_total = new_data_total;

    return OST_OK;
}

inline static
void conn_data_free(struct conn_data *d) {
    if(d->buf != NULL)
        free(d->buf);

    free(d);
}

inline static
int conn_ctx_get(struct ost_conn *c, int id, struct conn_ctx **ctx_ret) {
    size_t i;
    struct conn_ctx *ctx;

    for(i=0; i < c->n_contexts; ++i) {
        ctx = &((struct conn_ctx *)c->contexts)[i];
        if(ctx->id == id) {
            *ctx_ret = ctx;
            return OST_OK;
        }
    }

    return OST_ERROR;
}

inline static
int conn_ctx_ensure(struct ost_conn *c, int id, struct conn_ctx **ctx_ret) {
    int e;
    struct conn_ctx *ctx,
                    *tmp;
    size_t n_alloc;

    #define OST_CTX_ALLOC_POOL 4

    e = conn_ctx_get(c, id, &ctx);
    if(e == OST_ERROR) {
        if(c->n_contexts == c->n_contexts_alloc) {
            n_alloc = c->n_contexts_alloc+OST_CTX_ALLOC_POOL;
            tmp = realloc(c->contexts, sizeof(struct conn_ctx)*n_alloc);
            if(tmp == NULL)
                return OST_MALLOC;

            c->contexts = tmp;
            c->n_contexts_alloc = n_alloc;
            c->data_total += sizeof(struct conn_ctx)*OST_CTX_ALLOC_POOL;

            memset(c->contexts+c->n_contexts, 0, (sizeof(struct conn_ctx))*(c->n_contexts_alloc-c->n_contexts));
        }

        ctx = &((struct conn_ctx *)c->contexts)[c->n_contexts++];
        ctx->id = id;

        ost_debug("[ctx] create / %i\n", id);

        e = OST_OK;
    }

    if(ctx_ret != NULL)
        *ctx_ret = ctx;

    return OST_OK;
}

inline static
void conn_ctx_clear(struct ost_conn *c, struct conn_ctx *ctx) {
    struct conn_data *d,
                     *dn;

    for(d=ctx->data; d != NULL; d=dn) {
        dn = d->next;
        c->data_total -= sizeof(struct conn_data)+strlen(d->key)+1+d->size;
        conn_data_free(d);
    }

    ctx->status = -1;
    ctx->data = NULL;
    ctx->data_last = NULL;
    ctx->pending_rsp_count = 0;
}

inline static
void conn_clear(struct ost_conn *c) {
    size_t i;
    struct conn_ctx *cs;

    for(i=0; i < c->n_contexts; ++i)
        conn_ctx_clear(c, &c->contexts[i]);
}

//
// outgoing workers
//

struct conn_worker_out {
    ost_conn_worker_out_cb *callback;
    void *aux;
    struct conn_worker_out *prev,
                           *next;
};

inline static
int workers_out_add(struct ost_conn *c, ost_conn_worker_out_cb *callback, void *aux) {
    struct conn_worker_out *w,
                           *last;

    w = malloc(sizeof(struct conn_worker_out));
    if(w == NULL)
        return OST_MALLOC;

    w->callback = callback;
    w->aux = aux;

    last = c->workers_out_last;
    if(last != NULL) {
        w->prev = last;
        last->next = w;
        c->workers_out = w;
        c->workers_out_last = w;
    }
    else {
        w->prev = NULL;
        c->workers_out = w;
        c->workers_out_last = w;
    }

    w->next = NULL;

    return OST_OK;
}

inline static
void workers_out_rm(struct ost_conn *c, struct conn_worker_out *w) {
    if(w == c->workers_out)
        c->workers_out = w->next;
    if(w == c->workers_out_last)
        c->workers_out_last = w->prev;

    if(w->prev != NULL)
        w->prev->next = w->next;
    if(w->next != NULL)
        w->next->prev = w->prev;

    free(w);
}

inline static
void workers_out_iter(struct ost_conn *c) {
    int e;
    struct conn_worker_out *w,
                           *wn;

    for(w=c->workers_out; w != NULL; w=wn) {
        wn = w->next;
        e = w->callback(c, w->aux);
        if(e == OST_STOP)
            workers_out_rm(c,w);
    }
}

//
// key exchange
//

inline static
int keyx_slave(struct ost_conn *c) {
    int e,
        rsp;
    bool rekey;
    struct conn_ctx *ctx;
    size_t cmd_len,
           p_size,
           Y_size,
           X_size,
           max_param;
    char cmd[16];
    void *p_buf,
         *Y_buf,
         *X_buf;
    mpz_t s;
#ifdef DEBUG
    size_t i;
#endif

    rekey = c->secure.state == OST_SECURE_ACTIVE;
    if(rekey)
        c->secure.keyx.in_progress = 2;
    else
        c->secure.keyx.in_progress = 1;

    // ==========
    // slave/init
    // ==========

    ost_debug("[keyx] \e[94m-- slave/init --\e[0m\n");

    e = conn_ctx_ensure(c, KEYX_CTX, &ctx);
    if(e != OST_OK)
        goto e0;

    ctx->pending_rsp_count = 1;

    c->secure.prev_state = c->secure.state;
    c->secure.state = OST_SECURE_KEYX_READY;

    // request parameter(s) from master

    cmd_len = snprintf(cmd, sizeof(cmd), "%s %i\n", rekey ? "rekey" : "keyx", ctx->id);
    ost_queue(c, cmd, cmd_len);

    // verify response

    if(ost_sync(c) != OST_OK ||
       ctx->status != OST_RSP_CONTINUE)
    {
        ost_debug("[keyx] \e[91mmaster/init failed\e[0m\n");
        e = OST_ERROR;
        rsp = -1;
        goto e1;
    }

    ctx->pending_rsp_count = 0;

    // ==========
    // slave/main
    // ==========

    ost_debug("[keyx] \e[94m-- slave/main --\e[0m\n");
    c->secure.state = OST_SECURE_KEYX_MAIN;

    // import parameters

    if(rekey == false) {
        if(ost_get_uint_data(c, ctx->id, "buf-size", &c->secure.keyx.peer_buf_size) != OST_OK ||
           ost_get_uint_data(c, ctx->id, "key-size", &c->secure.keyx.key_size) != OST_OK ||
           ost_get_uint_data(c, ctx->id, "x-size", &c->secure.keyx.x_size) != OST_OK ||
           ost_get_uint_data(c, ctx->id, "g", &c->secure.keyx.g) != OST_OK ||
           ost_get_data(c, ctx->id, "p", &p_buf, &p_size) != OST_OK)
        {
            e = OST_BAD_INPUT;
            rsp = OST_RSP_BAD_INPUT;
            goto e1;
        }

    #ifdef DEBUG_DH
        ost_debug("[keyx] buf size = \e[97m%lu\e[0m\n", c->secure.keyx.peer_buf_size);
        ost_debug("[keyx] key size = \e[97m%zu\e[0m\n", c->secure.keyx.key_size);
        ost_debug("[keyx] x size = \e[97m%zu\e[0m\n", c->secure.keyx.x_size);
        ost_debug("[keyx] g = \e[97m%lu\e[0m\n", c->secure.keyx.g);
    #endif

        mpz_import(c->secure.keyx.p, p_size, 1, 1, 1, 0, p_buf);

    #ifdef DEBUG_DH
        gmp_fprintf(stderr, "[keyx] p = \e[97m%Zx\e[0m\n", c->secure.keyx.p);
    #endif
    }

    if(ost_get_data(c, ctx->id, "Y", &Y_buf, &Y_size) != OST_OK) {
        e = OST_BAD_INPUT;
        rsp = OST_RSP_BAD_INPUT;
        goto e1;
    }

    mpz_import(c->secure.keyx.Y, Y_size, 1, 1, 1, 0, Y_buf);
#ifdef DEBUG_DH
    gmp_fprintf(stderr, "[keyx] Y = \e[97m%Zx\e[0m\n", c->secure.keyx.Y);
#endif

    // generate private(x) and public(X) values

    nc_random_mpz(c->secure.keyx.x_size, c->secure.keyx.x);
    nc_dh_gen_public(c->secure.keyx.g, c->secure.keyx.x, c->secure.keyx.p, c->secure.keyx.X);
#ifdef DEBUG_DH
    gmp_fprintf(stderr, "[keyx] x = \e[97m%Zx\e[0m\n", c->secure.keyx.x);
    gmp_fprintf(stderr, "[keyx] X = \e[97m%Zx\e[0m\n", c->secure.keyx.X);
#endif

    // generate secret value and derive key

    mpz_init(s);
    nc_dh_gen_secret(c->secure.keyx.Y, c->secure.keyx.x, c->secure.keyx.p, s);
    nc_key_derived_from_mpz(&c->secure.tmp_key, c->secure.keyx.key_size, s);
#ifdef DEBUG_DH
    gmp_fprintf(stderr, "[keyx] s = \e[97m%Zx\e[0m\n", s);
#endif
    mpz_clear(s);

#ifdef DEBUG_DH
    ost_debug("[keyx] key = \e[97m");
    for(i=0; i < c->secure.tmp_key.size; ++i)
        ost_debug("%02hx", c->secure.tmp_key.buf[i]);
    ost_debug("\e[0m\n");
#endif

    // export public(X) value

    X_size = mpz_byte_size(c->secure.keyx.X);
    X_buf = malloc(X_size);
    if(X_buf == NULL) {
        e = OST_MALLOC;
        // we can still send an error response if we ran out of memory
        // since output buffers are pre-allocated.
        rsp = OST_RSP_ERROR;
        goto e2;
    }

    // queue parameters

    if(rekey == false)
        ost_uint_data(c, ctx->id, "buf-size", c->limits.buffer_size);

    mpz_export(X_buf, &X_size, 1, 1, 1, 0, c->secure.keyx.X);
    ost_data(c, ctx->id, "Y", X_buf, X_size);
    free(X_buf);

    // send to master

    ost_status(c, ctx->id, OST_RSP_OK);
    ost_commit(c, ctx->id, NULL);
    ost_flush(c);

    // apply key

    e = ost_conn_configure_key(c, &c->secure.tmp_key);
    if(e != OST_OK) {
        // we already sent a response so it's impossible to notify that
        // this failed but if it did we have bigger problems anyway.
        rsp = -1;
        goto e2;
    }

    c->secure.prev_state = -1;
    c->secure.keyx.slave = true;
    c->secure.keyx.in_progress = 0;

    nc_key_clear(&c->secure.tmp_key);

    ost_debug("[keyx] \e[94m-- key exchange complete --\e[0m\n");

    return OST_OK;

e2: nc_key_clear(&c->secure.tmp_key);

e1: c->secure.state = c->secure.prev_state;
    c->secure.prev_state = -1;
    c->secure.keyx.in_progress = 0;

    if(rsp != -1) {
        ost_status(c, ctx->id, rsp);
        ost_commit(c, ctx->id, NULL);
    }

e0: return e;
}

inline static
int keyx_master_init(struct ost_conn *c, int ctx_id, bool rekey) {
    int e;
    size_t p_size,
           X_size;
    void *p_buf,
         *X_buf;

    ost_debug("[keyx] \e[94m-- master/init --\e[0m\n");

    // state checks and preconfiguration

    if(c->secure.keyx.in_progress) {
        e = OST_ERROR;
        goto e0;
    }

    if(rekey)
        c->secure.keyx.in_progress = 2;
    else
        c->secure.keyx.in_progress = 1;

    if(rekey == false && c->secure.state == OST_SECURE_KEYX_NULL) {
        if(c->server == NULL || c->server->secure.state != OST_SECURE_KEYX_READY) {
            e = OST_SECURE_KEYX_NULL;
            goto e0;
        }

        // use server keyx parameters as fallback

        ost_conn_configure_keyx(c, c->server->secure.keyx.key_size,
                                   c->server->secure.keyx.x_size,
                                   c->server->secure.keyx.g,
                                   c->server->secure.keyx.p);

        ost_debug("[keyx] using server params\n");
    }

    c->secure.keyx.in_progress = true;
    c->secure.prev_state = c->secure.state;

    // generate private(x) and public(X)

    nc_random_mpz(c->secure.keyx.x_size, c->secure.keyx.x);
    nc_dh_gen_public(c->secure.keyx.g, c->secure.keyx.x, c->secure.keyx.p, c->secure.keyx.X);

#ifdef DEBUG_DH
    gmp_fprintf(stderr, "[keyx] x = \e[97m%Zx\e[0m\n", c->secure.keyx.x);
    gmp_fprintf(stderr, "[keyx] X = \e[97m%Zx\e[0m\n", c->secure.keyx.X);
#endif

    // export parameters

    if(rekey == false) {
        ost_uint_data(c, ctx_id, "buf-size", c->limits.buffer_size);
        ost_uint_data(c, ctx_id, "key-size", c->secure.keyx.key_size);
        ost_uint_data(c, ctx_id, "x-size", c->secure.keyx.x_size);
        ost_uint_data(c, ctx_id, "g", c->secure.keyx.g);

        p_size = mpz_byte_size(c->secure.keyx.p);
        p_buf = malloc(p_size);
        if(p_buf == NULL) {
            e = OST_MALLOC;
            goto e1;
        }

        mpz_export(p_buf, &p_size, 1, 1, 1, 0, c->secure.keyx.p);
        ost_data(c, ctx_id, "p", p_buf, p_size);

        free(p_buf);
    }

    // queue public value

    X_size = mpz_byte_size(c->secure.keyx.X);
    X_buf = malloc(X_size);
    if(X_buf == NULL) {
        e = OST_MALLOC;
        goto e1;
    }

    mpz_export(X_buf, &X_size, 1, 1, 1, 0, c->secure.keyx.X);
    ost_data(c, ctx_id, "Y", X_buf, X_size);
    free(X_buf);

    // commit

    ost_status(c, ctx_id, OST_RSP_CONTINUE);
    ost_commit(c, ctx_id, NULL);

    c->secure.state = OST_SECURE_KEYX_MAIN;

    return OST_OK;

e1: c->secure.state = c->secure.prev_state;
    c->secure.prev_state = -1;
    c->secure.keyx.in_progress = 0;

e0: return e;
}

inline static
int keyx_master_main(struct ost_conn *c, int ctx_id) {
    int e;
    bool rekey;
    void *Y_buf;
    size_t Y_size;
    mpz_t s;

    rekey = c->secure.prev_state == OST_SECURE_ACTIVE;

    ost_debug("[keyx] \e[94m-- master/main --\e[0m\n");

    if(ost_get_status(c, ctx_id) != OST_RSP_OK) {
        e = OST_ERROR;
        goto e1;
    }

    if(rekey == false) {
        if(ost_get_uint_data(c, ctx_id, "buf-size", &c->secure.keyx.peer_buf_size) != OST_OK) {
            e = OST_BAD_INPUT;
            goto e1;
        }

        ost_debug("[keyx] buf size = \e[97m%lu\e[0m\n", c->secure.keyx.peer_buf_size);
    }

    if(ost_get_data(c, ctx_id, "Y", &Y_buf, &Y_size) != OST_OK) {
        e = OST_BAD_INPUT;
        goto e1;
    }

    // import slave public(Y) value

    mpz_import(c->secure.keyx.Y, Y_size, 1, 1, 1, 0, Y_buf);

    // generate secret value and derived key

    mpz_init(s);
    nc_dh_gen_secret(c->secure.keyx.Y, c->secure.keyx.x, c->secure.keyx.p, s);
    nc_key_derived_from_mpz(&c->secure.tmp_key, c->secure.keyx.key_size, s);
#ifdef DEBUG_DH
    gmp_fprintf(stderr, "[keyx] Y = \e[97m%Zx\e[0m\n", c->secure.keyx.Y);
    gmp_fprintf(stderr, "[keyx] s = \e[97m%Zx\e[0m\n", s);
#endif
    mpz_clear(s);

#ifdef DEBUG
    size_t i;
    ost_debug("[keyx] key = \e[97m");
    for(i=0; i < c->secure.tmp_key.size; ++i)
        ost_debug("%02hx", c->secure.tmp_key.buf[i]);
    ost_debug("\e[0m\n");
#endif

    // apply key

    e = ost_conn_configure_key(c, &c->secure.tmp_key);
    if(e != OST_OK)
        goto e2;

    c->secure.prev_state = -1;
    c->secure.keyx.slave = false;
    c->secure.keyx.in_progress = false;

    nc_key_clear(&c->secure.tmp_key);

    ost_debug("[keyx] \e[94m-- key exchange complete --\e[0m\n");

    if(rekey == false && c->main_in.len > 0)
        return OST_REITER;
    else
        return OST_OK;

e2: nc_key_clear(&c->secure.tmp_key);

e1: c->secure.state = c->secure.prev_state;
    c->secure.prev_state = -1;
    c->secure.keyx.in_progress = 0;

    return e;
}

//
// output
//

inline static
int enable_poll_out(struct ost_conn *c) {
    struct epoll_event ev;

    ev.events = EPOLL_EVENT_MASK|EPOLLOUT;
    ev.data.ptr = c;

    if(epoll_ctl(c->server->epoll_fd, EPOLL_CTL_MOD, c->fd, &ev) == -1) {
        perror("epoll_ctl()");
        return OST_ERROR;
    }

    return OST_OK;
}

inline static
int disable_poll_out(struct ost_conn *c) {
    struct epoll_event ev;

    ev.events = EPOLL_EVENT_MASK;
    ev.data.ptr = c;

    if(epoll_ctl(c->server->epoll_fd, EPOLL_CTL_MOD, c->fd, &ev) == -1) {
        perror("epoll_ctl()");
        return OST_ERROR;
    }

    return OST_OK;
}

inline static
int gen_payload_out(struct ost_conn *c) {
    int e;
    size_t max,
           orig_len,
           alignment,
           pad_size,
           padded_size,
           cmd_len,
           total_size,
           diff,
           n_blocks;
    char cmd[32];
    uint8_t *ptr,
            *hash;
#ifdef DEBUG
    size_t i;
#endif

#ifdef DEBUG_BUF
    ost_debug("\e[97m-- plain out --\e[0m\n");
    ost_buf_debug(&c->main_out);
#endif

    // calculate padded length

    orig_len = c->main_out.len;
    alignment = nc_pad_alignment(&c->secure.key, orig_len);
    pad_size = nc_pad_size(&c->secure.key, alignment, 1);
    padded_size = orig_len+pad_size;

    cmd_len = snprintf(cmd, sizeof(cmd), "payload %zu\n", padded_size);
    total_size = cmd_len+padded_size;

    // allocate+prep buffer

    if(c->aux_out.size < total_size) {
        e = ost_buf_realloc(&c->aux_out, total_size);
        if(e != OST_OK)
            return e;
    }

    e = ost_buf_append(&c->aux_out, cmd, cmd_len);
    if(e != OST_OK)
        return e;

    ptr = (uint8_t *)c->aux_out.tail;
    ost_buf_fill(&c->aux_out, &c->main_out, orig_len);

    // pad + hash + encrypt

#ifdef DEBUG
    ost_debug("[secure out] key(e) = \e[95m%zx\e[0m/\e[97m", c->secure.key.state);
    for(i=0; i < c->secure.key.size; ++i)
        ost_debug("%02hx", c->secure.key.buf[i]);
    ost_debug("\e[0m\n");
#endif

    e = nc_pad_auto(&c->secure.key, &ptr, orig_len, NULL, pad_size, &hash);
    if(e != NC_OK)
        return OST_ERROR;

    nc_hash_auto(&c->secure.key, ptr, orig_len, hash);
    n_blocks = nc_block_count(&c->secure.key, padded_size);
    nc_encrypt_auto(&c->secure.key, ptr, n_blocks);

    c->aux_out.len += pad_size;
    c->aux_out.rem -= pad_size;
    c->aux_out.tail += pad_size;

    ost_debug("[secure out] \e[95mencrypt\e[0m | \e[97m%zu\e[0m -> \e[97m%zu\e[0m bytes\n", orig_len, c->aux_out.len);

#ifdef DEBUG
    ost_debug("[secure out] key(d) = \e[95m%zx\e[0m/\e[97m", c->secure.key.state);
    for(i=0; i < c->secure.key.size; ++i)
        ost_debug("%02hx", c->secure.key.buf[i]);
    ost_debug("\e[0m\n");
#endif

    return OST_OK;
}

int iter_out_plain(struct ost_conn *c) {
    int e;

    while(c->main_out.len > 0) {
    #ifdef DEBUG_BUF
        ost_debug("\e[97m-- plain out --\e[0m\n");
        ost_buf_debug(&c->main_out);
        ost_debug("[plain out] \e[93msend\e[0m | \e[97m%zu\e[0m bytes\n", c->main_out.len);
    #endif

        e = ost_buf_send(&c->main_out, c, -1);
        if(e != OST_OK)
            return e;
    }

    return OST_OK;
}

int iter_out_secure(struct ost_conn *c) {
    int e;

    // generate a payload if there's queued data

    if(c->main_out.len > 0) {
        e = gen_payload_out(c);
        if(e != OST_OK)
            return e;
    }

    // flush secure buffer

    while(c->aux_out.len > 0) {
    #ifdef DEBUG_BUF
        ost_debug("\e[97m-- secure out --\e[0m\n");
        ost_buf_debug(&c->aux_out);
        ost_debug("[secure out] \e[93msend\e[0m | \e[97m%zu\e[0m bytes\n", c->aux_out.len);
    #endif

        e = ost_buf_send(&c->aux_out, c, -1);
        if(e != OST_OK)
            return e;
    }

    return OST_OK;
}

inline static
int process_poll_out(struct ost_conn *c) {
    int e;

    workers_out_iter(c);

    e = c->iter_out_func(c);
    if(e != OST_OK)
        return e;

    if(c->flush_out) {
        c->flush_out = false;
        ost_debug("%s: flush out \e[91mdisabled\e[0m\n", c->host);
    }

    if(c->workers_out == NULL) {
        // disable poll out on success if no workers are left
        ost_debug("%s: poll out \e[91mdisabled\e[0m\n", c->host);
        e = disable_poll_out(c);
        if(e != OST_OK)
            return e;
    }

    return OST_OK;
}

//
// input
//

inline static
int extract_command(struct ost_buf *src, char ***argv_ret, size_t *argc_ret) {
    int e;
    struct strtoken_state ts;
    char *buf,
         *cmd,
         **argv;
    size_t len,
           argc;

    strtoken_init(&ts, 0);
    buf = (char *)src->head;

    e = strtoken(buf, src->len, &ts, "\n", &cmd, &len);
    if(e == 1) {
        if(src->rem == 0)
            return OST_CHOKE;
        else
            return OST_PARTIAL;
    }

    argv = strsplitq(strstrip(cmd, len, NULL, false, NULL), " ", &argc);
    if(argv == NULL)
        return OST_MALLOC;

    ost_buf_ltrim(src, len+1);

    *argv_ret = argv;
    *argc_ret = argc;

    return OST_OK;
}

inline static
int process_command_in(struct ost_conn *c, struct ost_buf *src) {
    int e,
        ctx_id,
        rsp;
    char **argv,
         *end;
    size_t argc,
           size;
    struct conn_ctx *ctx;
    struct ost_query_data *q;

    e = extract_command(src, &argv, &argc);
    if(e != OST_OK)
        goto r0;

    if(argc < 2) {
        e = OST_BAD_CMD;
        goto r1;
    }

    errno = 0;
    ctx_id = strtoll(argv[1], &end, 10);
    if(errno != 0 || end == argv[1]) {
        e = OST_BAD_CTX;
        goto r1;
    }

    e = conn_ctx_ensure(c, ctx_id, &ctx);
    if(e != OST_OK)
        goto r1;

    if(strcmp(argv[0], "data") == 0) {
        if(argc > 4) {
            e = OST_BAD_INPUT;
            goto r1;
        }

        errno = 0;
        size = strtoull(argv[3], &end, 0);
        if(errno != 0 || end == argv[3]) {
            e = OST_BAD_INPUT;
            goto r1;
        }

        ost_debug("[%i: data in] key = \e[97m%s\e[0m, size = \e[97m%zu\e[0m\n", ctx->id, argv[2], size);

        e = conn_data_prep_fill(c, ctx, argv[2], size);
        if(e != OST_OK)
            goto r1;
    }
    else if(strcmp(argv[0], "status") == 0) {
        if(argc > 3) {
            e = OST_BAD_INPUT;
            goto r1;
        }

        errno = 0;
        ctx->status = strtoul(argv[2], &end, 10);
        if(errno != 0 || end == argv[2]) {
            e = OST_BAD_INPUT;
            goto r1;
        }

        ost_debug("[%i: status in] \e[97m%i\e[0m %s\n", ctx->id, ctx->status, ost_status_str[ctx->status]);
    }
    else if(strcmp(argv[0], "commit") == 0) {
        if(argc > 3) {
            e = OST_BAD_INPUT;
            goto r1;
        }

        if(argc == 3) {
            ost_debug("[%i: commit in] \e[97m%s\e[0m\n", ctx->id, argv[2]);

            if(c->server != NULL) {
                printf("%s: [commit] %i %s\n", c->host, ctx->id, argv[2]);

                e = ost_server_process_query(c->server, argv[2], &q);
                if(e == OST_OK) {
                    q->route->callback(c, ctx->id, q, q->route->aux);
                    if(q->route->temp)
                        route_destroy(q->route);

                    query_data_destroy(q);
                }
                else {
                    ost_status(c, ctx->id, OST_RSP_NOT_FOUND);
                    ost_commit(c, ctx->id, NULL);
                }
            }
            else {
                ost_status(c, ctx->id, OST_RSP_ERROR);
                ost_commit(c, ctx->id, NULL);

                ost_debug("[%i: commit in] not attached to server\n", ctx->id);
                e = OST_ERROR;
            }

            conn_ctx_clear(c, ctx);
        }
        else {
            ost_debug("[%i: commit in] \e[37mNULL\e[0m\n", ctx->id);

            if(ctx->status != OST_RSP_CONTINUE && ctx->pending_rsp_count > 0)
                --ctx->pending_rsp_count;

            if(c->server == NULL) {
                e = OST_COMMIT;
            }
            else if(c->secure.state == OST_SECURE_KEYX_MAIN) {
                e = keyx_master_main(c, ctx->id);
                if(e == OST_REITER) {
                    goto r1;
                }
                else if(e != OST_OK) {
                    e = OST_KEYX;
                    goto r1;
                }
            }
        }
    }
    else if(strcmp(argv[0], "keyx") == 0) {
        if(keyx_master_init(c, ctx->id, false) != OST_OK)
            e = OST_KEYX;
    }
    else if(strcmp(argv[0], "rekey") == 0) {
        if(keyx_master_init(c, ctx->id, true) != OST_OK)
            e = OST_KEYX;
    }
    else {
        ost_debug("[%i: bad command in] %s\n", ctx->id, argv[0]);
        e = OST_BAD_CMD;
    }

r1: strbuf_free(argv, argc);

r0: return e;
}

inline static
int process_plain_in(struct ost_conn *c, struct ost_buf *src) {
    int e;
    struct conn_ctx *ctx;
    struct conn_data *data;

    while(src->len > 0) {
        if(c->data_in_fill) {
            ost_buf_fill(&c->data_in, src, -1);
            ost_debug("[%i: data in] \e[96mfill\e[0m | \e[97m%zu\e[0m/\e[97m%zu\e[0m bytes\n",
                      ((struct conn_ctx *)c->data_in_ctx)->id, c->data_in.len, c->data_in.size);

            if(c->data_in.rem == 0) {
                ctx = c->data_in_ctx;
                data = c->data_in_dest;

                c->data_in_fill = false;
                c->data_in_ctx = NULL;
                c->data_in_dest = NULL;

                ost_buf_drop(&c->data_in, &data->buf, &data->size);
                if(data->callback != NULL)
                    data->callback(c, ctx->id, data->buf, data->size, data->aux);
            }
        }
        else {
            e = process_command_in(c, src);
            if(e != OST_OK)
                return e;
        }
    }

    return OST_OK;
}

inline static
int prep_payload_in(struct ost_conn *c, size_t size) {
    int e;

    ost_debug("[payload in] size = \e[97m%zu\e[0m\n", size);
    if(size > c->secure.max_payload_size)
        return OST_PAYLOAD_SIZE;

    e = ost_buf_configure(&c->aux_in, size);
    if(e != OST_OK)
        return e;

    c->secure.payload_in_fill = true;

    return OST_OK;
}

inline static
int process_command_in_secure(struct ost_conn *c) {
    int e;
    char **argv,
         *end;
    size_t argc,
           size;

    e = extract_command(&c->main_in, &argv, &argc);
    if(e != OST_OK)
        return e;

    // parse arguments

    if(strcmp(argv[0], "payload") == 0) {
        errno = 0;
        size = strtoull(argv[1], &end, 0);
        if(errno != 0 || end == argv[1]) {
            e = OST_BAD_CMD;
            goto r1;
        }

        e = prep_payload_in(c, size);
    }
    else {
        e = OST_BAD_CMD;
    }

r1: strbuf_free(argv, argc);

r0: return e;
}

inline static
int decrypt_payload_in(struct ost_conn *c) {
    int e;
    size_t n_blocks,
           pad_size,
           orig_len;
    uint8_t *hash;
#ifdef DEBUG
    size_t i;
#endif

    n_blocks = nc_block_count(&c->secure.key, c->aux_in.len);

    nc_key_advance(&c->secure.key, n_blocks);
    nc_key_save(&c->secure.key);

#ifdef DEBUG
    ost_debug("[payload in] key(d) = \e[95m%zx\e[0m/\e[97m", c->secure.key.state);
    for(i=0; i < c->secure.key.size; ++i)
        ost_debug("%02hx", c->secure.key.buf[i]);
    ost_debug("\e[0m\n");
#endif

    nc_decrypt_auto(&c->secure.key, (uint8_t *)c->aux_in.head, n_blocks);
    if(nc_depad_auto(&c->secure.key, (uint8_t *)c->aux_in.head, c->aux_in.len, &pad_size, &hash) != NC_OK) {
        e = OST_BAD_PAYLOAD;
        goto e1;
    }

    orig_len = c->aux_in.len-pad_size;
    ost_debug("[payload in] \e[95mdecrypt\e[0m | \e[97m%zu\e[0m -> \e[97m%zu\e[0m bytes\n", c->aux_in.len, orig_len);

    e = nc_hash_verify_auto(&c->secure.key, hash, (uint8_t *)c->aux_in.head, orig_len);
    if(e == NC_MALLOC) {
        e = OST_MALLOC;
        goto e1;
    }
    else if(e != NC_OK) {
        e = OST_BAD_PAYLOAD;
        goto e1;
    }

    ost_debug("[payload in] \e[92mverify\e[0m\n");

#ifdef DEBUG
    ost_debug("[payload in] key(e) = \e[95m%zx\e[0m/\e[97m", c->secure.key.state);
    for(i=0; i < c->secure.key.size; ++i)
        ost_debug("%02hx", c->secure.key.buf[i]);
    ost_debug("\e[0m\n");
#endif

    ost_buf_rtrim(&c->aux_in, pad_size);
    nc_key_restore(&c->secure.key);

    c->secure.payload_in_avail = true;

    return OST_OK;

e1: nc_key_restore(&c->secure.key);
    ost_buf_clear(&c->aux_in);

    return e;
}

int iter_in_plain(struct ost_conn *c) {
    int e;

#ifdef DEBUG_BUF
    ost_debug("\e[97m-- plain in --\e[0m\n");
    ost_buf_debug(&c->main_in);
    ost_debug("[plain in] \e[33mrecv\e[0m | \e[97m%zu\e[0m bytes\n", c->main_in.len);
#endif

    e = process_plain_in(c, &c->main_in);
    if(e != OST_OK)
        return e;

    return OST_OK;
}

int iter_in_secure(struct ost_conn *c) {
    int e;

    if(c->main_in.len > 0)
        goto fill;
    else if(c->secure.payload_in_avail)
        goto process;

#ifdef DEBUG_BUF
    ost_debug("\e[97m-- secure in --\e[0m\n");
    ost_buf_debug(&c->main_in);
    ost_debug("[secure in] \e[33mrecv\e[0m | \e[97m%zu\e[0m bytes\n", c->main_in.len);
#endif

    while(c->main_in.len > 0) {
    fill:
        if(c->secure.payload_in_fill) {
            ost_buf_fill(&c->aux_in, &c->main_in, -1);

            ost_debug("[payload in] \e[96mfill\e[0m | \e[97m%zu\e[0m/\e[97m%zu\e[0m bytes\n",
                      c->aux_in.len, c->aux_in.size);

            if(c->aux_in.rem == 0) {
                c->secure.payload_in_fill = false;

                e = decrypt_payload_in(c);
                if(e != OST_OK)
                    return e;

            process:
            #ifdef DEBUG_BUF
                ost_debug("\e[97m-- plain in --\e[0m\n");
                ost_buf_debug(&c->aux_in);
                ost_debug("[plain in] \e[33mrecv\e[0m | \e[97m%zu\e[0m bytes\n", c->aux_in.len);
            #endif

                e = process_plain_in(c, &c->aux_in);
                if(e == OST_PARTIAL)
                    return OST_BAD_INPUT;
                else if(e != OST_OK)
                    return e;

                if(c->aux_in.len == 0)
                    c->secure.payload_in_avail = false;
            }
        }
        else {
            e = process_command_in_secure(c);
            if(e != OST_OK)
                return e;
        }
    }

    return OST_OK;
}

inline static
int process_poll_in(struct ost_conn *c) {
    int e;
    size_t rem;

    e = ost_buf_recv(&c->main_in, c, -1);
    if(e != OST_OK)
        return e;

reiter:
    e = c->iter_in_func(c);
    ost_debug("[iter in] (%i) %s\n", e, ost_error_str[e]);

    if(e == OST_REITER) {
        // OST_REITER is returned if there was still data queued in
        // c->main_in after the initial key exchange and triggers a
        // full return from iter_in_plain() so that iter_in_secure()
        // can take over.
        goto reiter;
    }
    else if(e != OST_OK &&
            e != OST_PARTIAL &&
            e != OST_BAD_QUERY)
    {
        return e;
    }

    e = c->iter_out_func(c);
    if(e == OST_BLOCK) {
        e = enable_poll_out(c);
        if(e != OST_OK)
            return e;

        ost_debug("[server] poll out \e[92menabled\e[0m\n");

        c->flush_out = true;
        ost_debug("[server] flush out \e[92menabled\e[0m\n");
    }
    else if(e != OST_OK) {
        return e;
    }

    return OST_OK;
}

//
// buffers
//

int ost_buf_configure(struct ost_buf *buf, size_t size) {
    int e;

    ost_buf_clear(buf);

    e = ost_buf_realloc(buf, size);
    if(e != OST_OK)
        return e;

    buf->tail = buf->head;
    buf->len = 0;
    buf->rem = size;

    return OST_OK;
}

int ost_buf_realloc(struct ost_buf *buf, size_t size) {
    int e;
    char *tmp;
    size_t rem_cutoff;

    if(size > 0) {
        tmp = realloc(buf->head, size);
        if(tmp == NULL)
            return OST_MALLOC;
    }
    else {
        if(buf->head == NULL)
            return OST_OK;

        free(buf->head);
        tmp = NULL;
    }

    buf->head = tmp;
    buf->size = size;

    if(buf->len > buf->size) {
        buf->len = buf->size;
        buf->rem = 0;
    }
    else {
        buf->rem = buf->size-buf->len;
    }

    buf->tail = buf->head+buf->len;

    return OST_OK;
}

int ost_buf_grow(struct ost_buf *buf, size_t n) {
    int e;
    size_t len;

    len = buf->len+n;
    if(len > buf->size) {
        e = ost_buf_realloc(buf, len);
        if(e != OST_OK)
            return e;
    }

    return OST_OK;
}

void ost_buf_clear(struct ost_buf *buf) {
    memset(buf->head, 0, buf->len);

    buf->tail = buf->head;
    buf->len = 0;
    buf->rem = buf->size;
}

void ost_buf_dealloc(struct ost_buf *buf) {
    if(buf->head != NULL)
        free(buf->head);

    memset(buf, 0, sizeof(struct ost_buf));
}

void ost_buf_ltrim(struct ost_buf *buf, size_t n) {
    if(n > buf->len)
        n = buf->len;

    if(n > 0) {
        memmove(buf->head, buf->head+n, buf->len-n);

        buf->tail -= n;
        buf->len -= n;
        buf->rem += n;

        memset(buf->tail, 0, n);
    }
}

void ost_buf_rtrim(struct ost_buf *buf, size_t n) {
    if(n > buf->len)
        n = buf->len;

    if(n > 0) {
        buf->tail -= n;
        buf->len -= n;
        buf->rem += n;

        memset(buf->tail, 0, n);
    }
}

int ost_buf_send(struct ost_buf *buf, struct ost_conn *c, ssize_t n) {
    int e;

    if(n < 0 || n > buf->len)
        n = buf->len;

    if(n > 0) {
        errno = 0;
        n = send(c->fd, buf->head, buf->len, MSG_NOSIGNAL);
        if(n == -1) {
            if(errno == EAGAIN || errno == EWOULDBLOCK)
                return OST_BLOCK;
            else
                return OST_SEND;
        }
        else if(n == 0) {
            return OST_HANG_UP;
        }

        memmove(buf->head, buf->head+n, buf->len-n);

        buf->tail -= n;
        buf->len -= n;
        buf->rem += n;

        memset(buf->tail, 0, n);
    }

    return OST_OK;
}

int ost_buf_recv(struct ost_buf *buf, struct ost_conn *c, ssize_t n) {
    int e;

    if(n < 0 || n > buf->rem)
        n = buf->rem;

    if(n > 0) {
        errno = 0;
        n = recv(c->fd, buf->tail, buf->rem, MSG_NOSIGNAL);
        if(n == -1) {
            if(errno == EAGAIN || errno == EWOULDBLOCK)
                return OST_BLOCK;
            else
                return OST_RECV;
        }
        else if(n == 0) {
            return OST_HANG_UP;
        }

        buf->tail += n;
        buf->len += n;
        buf->rem -= n;
    }

    return OST_OK;
}

int ost_buf_fwrite(struct ost_buf *buf, FILE *fh, ssize_t n) {
    int e;

    if(n < 0 || n > buf->len)
        n = buf->len;

    n = fwrite(buf->head, 1, n, fh);
    if(n == -1)
        return OST_WRITE;

    ost_buf_ltrim(buf, n);

    return OST_OK;
}

int ost_buf_fread(struct ost_buf *buf, FILE *fh, ssize_t n) {
    int e;
    ssize_t nr;

    if(n < 0 || n > buf->rem)
        n = buf->rem;

    n = fread(buf->tail, 1, n, fh);
    if(n == -1)
        return OST_READ;

    buf->tail += n;
    buf->len += n;
    buf->rem -= n;

    return OST_OK;
}

size_t ost_buf_fill(struct ost_buf *dest, struct ost_buf *src, ssize_t n) {
    if(dest->rem == 0)
        return OST_BLOCK;

    if(n < 0)
        n = dest->rem;
    if(n > src->len)
        n = src->len;

    memcpy(dest->tail, src->head, n);
    dest->tail += n;
    dest->len += n;
    dest->rem -= n;

    memmove(src->head, src->head+n, src->len-n);
    src->tail -= n;
    src->len -= n;
    src->rem += n;

    memset(src->tail, 0, n);

    return n;
}

int ost_buf_append(struct ost_buf *buf, char *data, ssize_t n) {
    int e;
    size_t len;

    if(n == -1)
        n = strlen(data);

    if(n > 0) {
        e = ost_buf_grow(buf, n);
        if(e != OST_OK)
            return e;

        memcpy(buf->tail, data, n);

        buf->tail += n;
        buf->len += n;
        buf->rem -= n;
    }

    return OST_OK;
}

void ost_buf_drop(struct ost_buf *buf, char **data_ret, size_t *len_ret) {
    *data_ret = buf->head;
    if(len_ret != NULL)
        *len_ret = buf->len;

    memset(buf, 0, sizeof(struct ost_buf));
}

//
// file streaming
//

struct stream_in_state {
    FILE *fh;
    size_t len,
           rem;
};

struct stream_out_state {
    unsigned int ctx;
    struct ost_buf buf;
    FILE *fh;
    size_t len,
           rem;
};

inline static
void stream_in_state_free(struct stream_in_state *ss) {
    fclose(ss->fh);
    free(ss);
}

inline static
void stream_out_state_free(struct stream_out_state *ss) {
    ost_buf_dealloc(&ss->buf);
    fclose(ss->fh);
    free(ss);
}

int stream_in_iter(struct ost_conn *c, int ctx_id, char *data, size_t len, void *aux) {
    struct stream_in_state *ss;

    ss = aux;

    if(ss->rem > 0) {
        fwrite(data, 1, len, ss->fh);

        ss->len += len;
        ss->rem -= len;
    }

    if(ss->rem == 0) {
        ost_status(c, ctx_id, OST_RSP_OK);
        ost_commit(c, ctx_id, NULL);

        fclose(ss->fh);
        stream_in_state_free(ss);

        return OST_STOP;
    }

    return OST_OK;

e1: ost_status(c, ctx_id, OST_RSP_ERROR);
    ost_commit(c, ctx_id, NULL);

    return OST_STOP;
}

int stream_out_iter(struct ost_conn *c, void *aux) {
    struct stream_out_state *ss;

    ss = aux;

    if(ss->rem > 0) {
        ost_debug("[stream out] %zu bytes left\n", ss->rem);

        ost_buf_fread(&ss->buf, ss->fh, ss->rem);
        ost_data(c, ss->ctx, "body", ss->buf.head, ss->buf.len);

        ss->rem -= ss->buf.len;
        ost_buf_clear(&ss->buf);
    }

    if(ss->rem == 0) {
        ost_status(c, ss->ctx, OST_RSP_OK);
        ost_commit(c, ss->ctx, NULL);

        ost_debug("[stream out] complete\n");
        stream_out_state_free(ss);

        return OST_STOP;
    }

    return OST_OK;

e1: ost_status(c, ss->ctx, OST_RSP_ERROR);
    ost_commit(c, ss->ctx, NULL);

    return OST_STOP;
}

//
// server
//

int ost_server_init(struct ost_server *s, int port, int timeout, struct ost_limits *limits) {
    int e;

    memset(s, 0, sizeof(struct ost_server));

    if(port < 0)
        s->port = DEFAULT_PORT;
    else
        s->port = port;

    s->epoll_fd = -1;
    s->timeout = timeout;

    if(limits != NULL)
        ost_server_set_limits(s, limits);
    else
        ost_server_set_limits(s, &default_server_limits);

    mpz_init(s->secure.keyx.p);

    return OST_OK;
}

void ost_server_deinit(struct ost_server *s) {
    struct ost_conn *c,
                    *cn;
    struct ost_route *r,
                     *rn;

    for(c=s->connections; c != NULL; c=cn) {
        cn = c->next;
        ost_conn_destroy(c);
    }

    for(r=s->routes; r != NULL; r=rn) {
        rn = r->next;
        route_destroy(r);
    }

    if(s->secure.state == OST_SECURE_ACTIVE)
        nc_key_clear(&s->secure.key);

    mpz_clear(s->secure.keyx.p);
}

void ost_server_set_limits(struct ost_server *s, struct ost_limits *limits) {
    if(limits->buffer_size < 0)
        s->limits.buffer_size = DEFAULT_SERVER_BUFFER_SIZE;
    else if(limits->buffer_size >= MIN_BUFFER_SIZE)
        s->limits.buffer_size = limits->buffer_size;
    else
        s->limits.buffer_size = MIN_BUFFER_SIZE;

    if(limits->data_quota < 0)
        s->limits.data_quota = DEFAULT_SERVER_DATA_QUOTA;
    else if(limits->data_quota >= MIN_DATA_QUOTA)
        s->limits.data_quota = limits->data_quota;
    else
        s->limits.data_quota = MIN_DATA_QUOTA;
}

void ost_server_configure_keyx(struct ost_server *s, size_t key_size, size_t x_size, uint8_t g, mpz_t p) {
    s->secure.keyx.key_size = key_size;
    s->secure.keyx.x_size = x_size;
    s->secure.keyx.g = g;
    mpz_set(s->secure.keyx.p, p);

#ifdef DEBUG_DH
    ost_debug("[keyx] \e[93mconfigure\e[0m\n");
    ost_debug("[keyx] key size = \e[97m%lu\e[0m\n", s->secure.keyx.key_size);
    ost_debug("[keyx] x size = \e[97m%lu\e[0m\n", s->secure.keyx.x_size);
    ost_debug("[keyx] g = \e[97m%lu\e[0m\n", s->secure.keyx.g);
    gmp_fprintf(stderr, "[keyx] p = \e[97m%Zx\e[0m\n", s->secure.keyx.p);
#endif

    if(s->secure.state == OST_SECURE_KEYX_NULL)
        s->secure.state = OST_SECURE_KEYX_READY;
}

int ost_server_configure_key(struct ost_server *s, struct nc_key *k) {
    int e;
    size_t alignment,
           pad_size;

    e = nc_key_copy(&s->secure.key, k);
    if(e != NC_OK) {
        e = OST_MALLOC;
        return e;
    }

    s->secure.state = OST_SECURE_ACTIVE;

    return OST_OK;
}

void ost_server_attach_conn(struct ost_server *s, struct ost_conn *c) {
    c->prev = NULL;
    c->next = s->connections;
    s->connections = c;

    if(s->connections_last == NULL)
        s->connections_last = c;

    c->server = s;
}

void ost_server_detach_conn(struct ost_server *s, struct ost_conn *c) {
    if(c == s->connections)
        s->connections = c->next;
    if(c == s->connections_last)
        s->connections_last = c->prev;

    if(c->prev != NULL)
        c->prev->next = c->next;
    if(c->next != NULL)
        c->next->prev = c->prev;

    c->prev = NULL;
    c->next = NULL;
    c->server = NULL;
}

int ost_server_add_route(struct ost_server *s, char *tag, char *tpl, bool temp, ost_handler_cb *cb) {
    int e;
    struct ost_route *r;
    char *pt;

    r = malloc(sizeof(struct ost_route));
    if(r == NULL) {
        e = OST_MALLOC;
        goto e0;
    }

    r->tag = tag;
    r->tpl = strdup(tpl);
    if(r->tpl == NULL) {
        e = OST_MALLOC;
        goto e1;
    }

    pt = strstrip(r->tpl, -1, "/", false, NULL);

    r->tplv = strsplit(pt, "/", &r->tplc);
    if(r->tplv == NULL) {
        e = OST_MALLOC;
        goto e2;
    }

    r->temp = temp;
    r->callback = cb;

    route_attach(r,s);

    return OST_OK;

e3: strbuf_free(r->tplv, r->tplc);
e2: free(r->tpl);
e1: free(r);

e0: return e;
}

int ost_server_rm_route(struct ost_server *s, char *tag, char *tpl) {
    struct ost_route *r,
                     *rn;

    if(tag != NULL) {
        for(r=s->routes; r != NULL; r=rn) {
            rn = r->next;
            if(strcmp(r->tpl, tpl) == 0)
                route_destroy(r);
        }

        return OST_OK;
    }
    else {
        for(r=s->routes; r != NULL; r=rn) {
            rn = r->next;
            if(strcmp(r->tpl, tpl) == 0) {
                route_destroy(r);
                return OST_OK;
            }
        }

        return OST_ERROR;
    }
}

int ost_server_process_query(struct ost_server *s, char *query, struct ost_query_data **q_ret) {
    int e;
    struct ost_query_data *q;
    struct ost_route *r;
    bool match;
    size_t i;
    char *tk;
    struct ost_query_arg **target,
                         *qt;

    q = query_data_new(query);
    if(q == NULL) {
        e = OST_MALLOC;
        goto e0;
    }

    target = &q->tokens;

    for(r=s->routes; r != NULL; r=r->next) {
        if(r->tplc < q->pathc)
            continue;

        for(i=0; i < r->tplc; ++i) {
            if(r->tplv[i][0] == '$') {
                if(i < q->pathc)
                    tk = strbuf_join(q->pathv+i, q->pathc-i, "/", NULL, NULL);
                else
                    tk = strdup("");

                if(tk == NULL) {
                    e = OST_MALLOC;
                    goto e1;
                }

                qt = query_arg_new(r->tplv[i]+1, tk);
                if(qt == NULL) {
                    e = OST_MALLOC;
                    goto e2;
                }

                *target = qt;
                target = &qt->next;

                i = q->pathc;
                break;
            }
            else if(r->tplv[i][0] == ':') {
                tk = strdup(q->pathv[i]);
                if(tk == NULL) {
                    e = OST_MALLOC;
                    goto e1;
                }

                qt = query_arg_new(r->tplv[i]+1, tk);
                if(qt == NULL) {
                    e = OST_MALLOC;
                    goto e2;
                }

                *target = qt;
                target = &qt->next;
            }
            else if(strcmp(r->tplv[i], q->pathv[i]) != 0) {
                break;
            }
        }

        if(i == q->pathc) {
            ost_debug("[route] \e[97m%s\e[0m -> \e[97m%s\e[0m\n", q->path, r->tpl);
            q->route = r;
            break;
        }

        ost_query_args_destroy(q->tokens);
    }

    if(q->route == NULL) {
        ost_debug("[route] no matching template found\n");
        e = OST_BAD_QUERY;
        goto e1;
    }

    *q_ret = q;

    return OST_OK;

e2: free(tk);
e1: query_data_destroy(q);

e0: return e;
}

int ost_server_main(struct ost_server *s) {
    int e,
        listen_fd,
        conn_fd;
    char service[NI_MAXSERV];
    struct addrinfo hints,
                    *info,
                    *ai;
    struct sockaddr_storage saddr,
                            addr;
    socklen_t saddr_len,
              addr_len;
    struct epoll_event ev,
                       ev_buf[EPOLL_MAX_EVENTS];
    ssize_t n,
            nr;
    size_t i;
    struct ost_conn *c;
    struct conn_worker_out *w,
                           *wn;
    ptime_t now;
    unsigned int inactivity;

    memset(&hints, 0, sizeof(struct addrinfo));

    snprintf(service, NI_MAXSERV, "%u", s->port);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE|AI_NUMERICSERV;

    errno = 0;
    e = getaddrinfo(NULL, service, &hints, &info);
    if(e != 0) {
        e = OST_SYSTEM;
        goto r0;
    }

    for(ai=info; ai != NULL; ai=ai->ai_next) {
        errno = 0;
        listen_fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
        if(listen_fd == -1) {
            e = OST_SYSTEM;
            continue;
        }

        setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));

        errno = 0;

        if(bind(listen_fd, ai->ai_addr, ai->ai_addrlen) == -1) {
            e = OST_SYSTEM;
            goto next_addr;
        }

        if(listen(listen_fd, LISTEN_BACKLOG) == -1) {
            e = OST_SYSTEM;
            goto next_addr;
        }

        memcpy(&s->addr, ai->ai_addr, ai->ai_addrlen);
        s->addr_len = ai->ai_addrlen;

        e = getnameinfo((struct sockaddr *)&s->addr, s->addr_len,
                        s->host, sizeof(s->host), NULL, 0, NI_NUMERICHOST);

        if(e != 0) {
            e = OST_SYSTEM;
            goto r0;
        }

        printf("server: listening [%s/%u]\n", s->host, s->port);

        break;

    next_addr:
        close(listen_fd);
        listen_fd = -1;
    }

    freeaddrinfo(info);

    if(listen_fd == -1)
        goto r0;

    // create epoll fd

    errno = 0;
    s->epoll_fd = epoll_create1(0);
    if(s->epoll_fd == -1) {
        e = OST_SYSTEM;
        goto r1;
    }

    // add listen fd to epoll

    ev.events = EPOLL_EVENT_MASK;
    ev.data.ptr = &listen_fd;

    e = epoll_ctl(s->epoll_fd, EPOLL_CTL_ADD, listen_fd, &ev);
    if(e == -1) {
        e = OST_SYSTEM;
        goto r2;
    }

    // add any preattached connections to epoll

    for(c=s->connections; c != NULL; c=c->next) {
        ev.events = EPOLL_EVENT_MASK;
        ev.data.ptr = c;

        e = epoll_ctl(s->epoll_fd, EPOLL_CTL_ADD, c->fd, &ev);
        if(e == -1) {
            e = OST_SYSTEM;
            goto r2;
        }
    }

    // main server loop

    s->active = true;

    while(s->active) {
        n = epoll_wait(s->epoll_fd, ev_buf, EPOLL_MAX_EVENTS, 1000);
        if(n == -1) {
            if(errno == EINTR)
                continue;

            e = OST_POLL;
            s->active = false;
            break;
        }

        for(i=0; i < n; ++i) {
            if(ev_buf[i].data.ptr == &listen_fd) {
                // accept

                addr_len = sizeof(addr);
                conn_fd = accept(listen_fd, (struct sockaddr *)&addr, &addr_len);
                if(conn_fd == -1) {
                    e = OST_ACCEPT;
                    break;
                }

                // create connection

                e = ost_conn_new(conn_fd, s->port, s->timeout, &s->limits, &addr, addr_len, &c);
                if(e != OST_OK) {
                    close(conn_fd);
                    continue;
                }

                printf("%s: connected\n", c->host);

                // attach to server and apply key if preconfigured server-side

                ost_server_attach_conn(s,c);
                if(s->secure.state == OST_SECURE_ACTIVE) {
                    ost_conn_configure_keyx(c, s->secure.keyx.key_size, s->secure.keyx.x_size,
                                            s->secure.keyx.g, s->secure.keyx.p);

                    e = ost_conn_configure_key(c, &s->secure.key);
                    if(e != OST_OK)
                        goto ce;
                }

                // add connection to epoll

                ev.events = EPOLL_EVENT_MASK;
                ev.data.ptr = c;

                // add to epoll

                e = epoll_ctl(s->epoll_fd, EPOLL_CTL_ADD, conn_fd, &ev);
                if(e == -1) {
                    e = OST_SYSTEM;
                    goto ce;
                }
            }
            else {
                c = ev_buf[i].data.ptr;

                if(ev_buf[i].events & EPOLLRDHUP) {
                    printf("%s: disconnected\n", c->host);
                    ost_conn_destroy(c);
                    continue;
                }
                else if(ev_buf[i].events & EPOLLHUP) {
                    e = OST_HANG_UP;
                    goto ce;
                }

                if(ev_buf[i].events & EPOLLOUT) {
                    e = process_poll_out(c);
                    if(e != OST_OK)
                        goto ce;
                }

                if(ev_buf[i].events & EPOLLIN) {
                    e = process_poll_in(c);
                    if(e != OST_OK)
                        goto ce;
                }

                ost_server_detach_conn(s,c);
                ost_server_attach_conn(s,c);

                c->last_active = time_now(CLOCK_REALTIME);
            }
        }

        for(c=s->connections_last; c != NULL; c=c->prev) {
            if(c->timeout <= -1)
                continue;

            now = time_now(CLOCK_REALTIME);
            inactivity = (now-c->last_active)*1000;

            if(inactivity >= c->timeout) {
                ost_debug("[server] %s: inactivity = \e[97m%u\e[0mms\n", c->host, inactivity);
                ost_conn_close(c);
            }
        }

        continue;

    ce: printf("%s: %s\n", c->host, ost_error_str[e]);
        printf("%s: connection terminated\n", c->host);
        ost_conn_destroy(c);
    }

    e = OST_OK;

r2: close(s->epoll_fd);
r1: close(listen_fd);

    s->epoll_fd = -1;

r0: return e;
}

void ost_server_stop(struct ost_server *s) {
    s->active = false;
}

//
// connection
//

int ost_conn_new(int fd, unsigned int port, int timeout, struct ost_limits *limits,
                 struct sockaddr_storage *addr, socklen_t addr_len, struct ost_conn **conn_ret)
{
    int e,
        flags;
    struct ost_conn *c;

    c = malloc(sizeof(struct ost_conn));
    if(c == NULL)
        goto e0;

    memset(c, 0, sizeof(struct ost_conn));

    if(limits != NULL)
        ost_conn_set_limits(c, limits);
    else
        ost_conn_set_limits(c, &default_client_limits);

    if(ost_buf_configure(&c->main_in, c->limits.buffer_size) != OST_OK)
        goto e1;
    if(ost_buf_realloc(&c->main_out, c->limits.buffer_size) != OST_OK)
        goto e2;

    flags = fcntl(fd, F_GETFL, 0);
    fcntl(fd, F_SETFL, flags|O_NONBLOCK);

    c->fd = fd;
    c->timeout = timeout;
    c->port = port;

    if(addr_len > 0) {
        memcpy(&c->addr, addr, addr_len);
        c->addr_len = addr_len;

        e = getnameinfo((struct sockaddr *)&c->addr, c->addr_len,
                        c->host, sizeof(c->host), NULL, 0, NI_NUMERICHOST);

        if(e != 0) {
            e = OST_SYSTEM;
            goto e3;
        }
    }

    c->last_active = time_now(CLOCK_MONOTONIC);
    c->iter_in_func = iter_in_plain;
    c->iter_out_func = iter_out_plain;

    c->secure.state = OST_SECURE_KEYX_NULL;
    c->secure.prev_state = -1;
    c->secure.keyx.g = 2;
    mpz_inits(c->secure.keyx.x,
              c->secure.keyx.X,
              c->secure.keyx.Y,
              c->secure.keyx.p, NULL);

    *conn_ret = c;

    return OST_OK;

e3: ost_buf_dealloc(&c->main_out);
e2: ost_buf_dealloc(&c->main_in);
e1: free(c);

e0: return OST_MALLOC;
}

void ost_conn_close(struct ost_conn *c) {
    if(c->fd != -1) {
        shutdown(c->fd, SHUT_RDWR);
        close(c->fd);
        c->fd = -1;
    }
}

void ost_conn_destroy(struct ost_conn *c) {
    size_t i;
    struct conn_data *d,
                     *dn;

    ost_conn_close(c);

    ost_buf_dealloc(&c->main_in);
    ost_buf_dealloc(&c->main_out);
    ost_buf_dealloc(&c->data_in);
    ost_buf_dealloc(&c->aux_in);
    ost_buf_dealloc(&c->aux_out);

    for(i=0; i < c->n_contexts; ++i)
        conn_ctx_clear(c, &c->contexts[i]);

    free(c->contexts);

    mpz_clears(c->secure.keyx.x,
               c->secure.keyx.X,
               c->secure.keyx.Y,
               c->secure.keyx.p, NULL);

    if(c->secure.state == OST_SECURE_ACTIVE)
        nc_key_clear(&c->secure.key);

    // detach from server

    if(c->server != NULL)
        ost_server_detach_conn(c->server, c);

    free(c);
}

void ost_conn_configure_keyx(struct ost_conn *c, size_t key_size, size_t x_size, uint8_t g, mpz_t p) {
    c->secure.keyx.key_size = key_size;
    c->secure.keyx.x_size = x_size;
    c->secure.keyx.g = g;
    mpz_set(c->secure.keyx.p, p);

    if(c->secure.state == OST_SECURE_KEYX_NULL)
        c->secure.state = OST_SECURE_KEYX_READY;
}

int ost_conn_configure_key(struct ost_conn *c, struct nc_key *key) {
    int e;
    size_t p_size,
           Y_size,
           max_param,
           adj_param,
           pad_alignment,
           pad_size;

    e = nc_key_copy(&c->secure.key, key);
    if(e != NC_OK)
        return OST_MALLOC;

    // calculate max payload size

    if(c->secure.keyx.in_progress == 1) {
        p_size = mpz_byte_size(c->secure.keyx.p);
        Y_size = mpz_byte_size(c->secure.keyx.Y);

        max_param = p_size;
        if(Y_size > max_param)
            max_param = Y_size;
        if(c->secure.keyx.peer_buf_size > max_param)
            max_param = c->secure.keyx.peer_buf_size;

        adj_param = max_param+128;
        pad_alignment = nc_pad_alignment(&c->secure.key, adj_param);
        pad_size = nc_pad_size(&c->secure.key, pad_alignment, 1);

        c->secure.max_payload_size = adj_param+pad_size;

        ost_debug("[secure] max payload size = \e[97m%lu\e[0m\n", c->secure.max_payload_size);
    }

    // set iter functions

    c->iter_in_func = iter_in_secure;
    c->iter_out_func = iter_out_secure;

    // set state

    c->secure.state = OST_SECURE_ACTIVE;

    return OST_OK;
}

void ost_conn_set_limits(struct ost_conn *c, struct ost_limits *limits) {
    if(limits->buffer_size < 0)
        c->limits.buffer_size = DEFAULT_CLIENT_BUFFER_SIZE;
    else if(limits->buffer_size >= MIN_BUFFER_SIZE)
        c->limits.buffer_size = limits->buffer_size;
    else
        c->limits.buffer_size = MIN_BUFFER_SIZE;

    if(limits->data_quota < 0)
        c->limits.data_quota = DEFAULT_CLIENT_DATA_QUOTA;
    else if(limits->data_quota >= MIN_DATA_QUOTA)
        c->limits.data_quota = limits->data_quota;
    else
        c->limits.data_quota = MIN_DATA_QUOTA;
}

void ost_conn_set_timeout(struct ost_conn *c, int timeout) {
    c->timeout = timeout;
}

int ost_conn_set_worker_in(struct ost_conn *c, int ctx_id, char *key, ost_conn_worker_in_cb *callback, void *aux) {
    int e;
    struct conn_ctx *ctx;
    struct conn_data *data;

    e = conn_ctx_ensure(c, ctx_id, &ctx);
    if(e != OST_OK)
        return e;

    e = conn_data_ensure(c, ctx, key, &data);
    if(e != OST_OK)
        return e;

    data->callback = callback;
    data->aux = aux;

    return OST_OK;
}

int ost_conn_set_worker_out(struct ost_conn *c, ost_conn_worker_out_cb *callback, void *aux) {
    int e;

    e = enable_poll_out(c);
    if(e != OST_OK)
        return e;

    return workers_out_add(c, callback, aux);
}

int ost_conn_set_stream_in(struct ost_conn *c, int ctx_id, char *file_path, off_t offset, ssize_t len) {
    int e;
    struct stream_in_state *ss;

    ss = malloc(sizeof(struct stream_in_state));
    if(ss == NULL) {
        e = OST_MALLOC;
        goto e0;
    }

    ss->fh = fopen(file_path, "w+");
    if(ss->fh == NULL) {
        e = OST_OPEN;
        goto e1;
    }

    if(offset > 0) {
        fseek(ss->fh, offset, SEEK_SET);
        ss->rem = len;
    }
    else {
        ss->rem = len;
    }

    e = ost_conn_set_worker_in(c, ctx_id, "body", stream_in_iter, ss);
    if(e != OST_OK)
        goto e2;

    return OST_OK;

e2: fclose(ss->fh);
e1: free(ss);

e0: return e;
}

int ost_conn_set_stream_out(struct ost_conn *c, int ctx, char *file_path, off_t offset, ssize_t len, ssize_t chunk_size) {
    int e;
    struct stream_out_state *ss;
    size_t total_len,
           key_size;

    ss = malloc(sizeof(struct stream_out_state));
    if(ss == NULL) {
        e = OST_MALLOC;
        goto e0;
    }

    ss->ctx = ctx;

    ss->fh = fopen(file_path, "r");
    if(ss->fh == NULL) {
        e = OST_OPEN;
        goto e1;
    }

    fseek(ss->fh, 0, SEEK_END);
    total_len = ftell(ss->fh);

    if(len < 0)
        len = total_len;

    ost_debug("[stream out] offset = \e[97m%zu\e[0m, len = \e[97m%zi\e[0m\n", offset, len);

    if(offset+len > total_len) {
        e = OST_BAD_INPUT;
        goto e2;
    }

    fseek(ss->fh, offset, SEEK_SET);
    ss->rem = len;

    memset(&ss->buf, 0, sizeof(ss->buf));

    if(chunk_size < 0)
        chunk_size = c->limits.buffer_size;
    else if(chunk_size < MIN_BUFFER_SIZE)
        chunk_size = MIN_BUFFER_SIZE;

    e = ost_buf_configure(&ss->buf, chunk_size);
    if(e != OST_OK) {
        e = OST_MALLOC;
        goto e2;
    }

    e = ost_conn_set_worker_out(c, stream_out_iter, ss);
    if(e != OST_OK)
        goto e3;

    return OST_OK;

e3: ost_buf_dealloc(&ss->buf);
e2: fclose(ss->fh);
e1: free(ss);

e0: return e;
}

//
// url encoding/decoding/parsing
//

inline static
bool isreserved(char c) {
    return c == ':' || c == '/' || c == '?' || c == '#' ||
           c == '[' || c == ']' || c == '@' || c == '!' ||
           c == '$' || c == '&' || c == '\'' || c == '(' ||
           c == ')' || c == '*' || c == '+' || c == ',' ||
           c == ';' || c == '=';
}

inline static
bool isunsafe(char c) {
    return c == '<' || c == '>' || c == '%' || c == '{' ||
           c == '}' || c == '|' || c == '\\' || c == '^' ||
           c == '`' || c == '"' || isspace(c);
}

inline static
bool needs_encode(char c) {
    return isreserved(c) ||
           isunsafe(c) ||
           isprint(c) == false;
}

int ost_url_encode(char *str, char safe, char **str_ret) {
    int e,c;
    size_t len,
           elen,
           i,j,n;
    bool v;
    uint32_t code;
    char *estr,
         val_str[16];

    len = strlen(str);

    // calculate encoded length

    elen = 0;
    for(i=0; i < len; i += c) {
        e = utf8_decode_char(&str[i], &code, &c);
        if(e == UTF8_INVALID)
            return OST_BAD_URL;
        else if(c > 1 && needs_encode((char)code))
            elen += 1+(c*2); // '%' + 2-digit hex values
        else
            ++elen;
    }

    // allocate

    estr = malloc(elen+1);
    if(estr == NULL)
        return OST_MALLOC;

    estr[j] = '\0';

    // encode

    if(needs_encode(safe))
        safe = '_';

    for(i=0,j=0; i < len; i+=c,j+=n) {
        e = utf8_decode_char(&str[i], &code, &c);
        if(c > 1 || needs_encode((char)code)) {
            // multi-char
            n = snprintf(val_str, sizeof(val_str), "%0*X", c, code);
            memcpy(str+j, val_str, n);
        }
        else {
            // ascii
            estr[j] = str[i];
            n = 1;
        }
    }

    *str_ret = estr;

    return OST_OK;
}

int ost_url_decode(char *str, char **str_ret) {
    int c;
    size_t len,
           i,j,n;
    char *dstr,
         *start,
         *endptr;
    uint64_t code;

    len = strlen(str);

    dstr = malloc(len+1);
    if(dstr == NULL)
        return OST_MALLOC;

    for(i=0,j=0; str[i] != '\0'; i+=n,j+=c) {
        if(str[i] == '%') {
            ++i;

            start = str+i;
            code = strtoull(start, &endptr, 16);
            if(endptr == start)
                return OST_BAD_URL;

            n = endptr-start;
            if(utf8_encode_char(code, dstr+j, &c) != UTF8_OK)
                return OST_BAD_URL;
        }
        else {
            dstr[j] = str[i];

            n = 1;
            c = 1;
        }
    }

    dstr[j] = '\0';

    *str_ret = dstr;

    return OST_OK;
}

int ost_parse_url(char *url, char **host_ret, int *port_ret, char **query_ret) {
    int e,
        port;
    size_t n,
           qs;
    char *ptr,
         *host,
         *port_str,
         *end,
         *query;
    struct strtoken_state s;
    bool has_port,
         has_query;

    ptr = url;
    if(strscancmp(ptr, "ost://", &n) == 0)
        ptr += n;

    strtoken_init(&s, STRTOKEN_DUP);

    e = strtoken(ptr, -1, &s, ":", &host, NULL);
    if(e == -1) {
        e = OST_MALLOC;
        goto e0;
    }

    if(e == 0) {
        // host with port

        has_port = true;
        if(strtoken(ptr, -1, &s, "/", &port_str, NULL) == 0) {
            has_query = true;
        }
        else {
            has_query = false;
            if(strtoken(ptr, -1, &s, NULL, &port_str, NULL) == -1) {
                e = OST_MALLOC;
                goto e1;
            }
        }

        errno = 0;
        port = strtoull(port_str, &end, 10);
        if(errno != 0 || end == port_str) {
            e = OST_BAD_URL;
            goto e2;
        }

        free(port_str);
    }
    else {
        // host without port

        has_port = false;
        if(strtoken(ptr, -1, &s, "/", &host, NULL) == 0) {
            has_query = true;
        }
        else {
            has_query = false;
            if(strtoken(ptr, -1, &s, NULL, &host, NULL) == -1) {
                e = OST_MALLOC;
                goto e0;
            }
        }

        port = DEFAULT_PORT;
    }

    // query
    if(has_query) {
        qs = strtoken_unmask(ptr, &s);
        strtoken_set_next(&s, qs);

        if(strtoken(ptr, -1, &s, NULL, &query, NULL) == -1) {
            e = OST_MALLOC;
            goto e1;
        }
    }
    else {
        query = NULL;
    }

    strtoken_reset(ptr, &s);

    *host_ret = host;
    *port_ret = port;
    *query_ret = query;

    return OST_OK;

e2: free(port_str);
e1: free(host);

    strtoken_reset(ptr, &s);

e0: return e;
}

int ost_parse_query(char *query, char **path_ret, struct ost_query_arg **args_ret) {
    int e;
    struct strtoken_state s;
    struct ost_query_arg *root,
                         **target,
                         *qa;
    char *path,
         *args,
         **argv,
         *key,
         *arg1,
         *arg2;
    size_t argc,
           i;

    strtoken_init(&s, STRTOKEN_DUP);
    root = NULL;
    target = &root;

    e = strtoken(query, -1, &s, "?", &path, NULL);
    if(e == -1) {
        e = OST_MALLOC;
        goto e0;
    }
    else if(e == 0) {
        argv = strsplit(args, ",", &argc);
        if(argv == NULL) {
            e = OST_MALLOC;
            goto e1;
        }

        for(i=0; i < argc; ++i) {
            e = strtoken(argv[i], -1, &s, "=", &key, NULL);
            if(e == -1) {
                e = OST_MALLOC;
                goto e2;
            }
            else if(e == 0) {
                e = strtoken(argv[i], -1, &s, NULL, &arg1, NULL);
                if(e != OST_OK)
                    goto e3;

                e = ost_url_decode(arg1, &arg2);
                if(e != OST_OK)
                    goto e4;

                qa = query_arg_new(key, arg2);
                if(qa == NULL) {
                    e = OST_MALLOC;
                    goto e5;
                }

                *target = qa;
                target = &qa->next;

                free(key);
                free(arg1);
            }
            else {
                e = ost_url_decode(argv[i], &arg1);
                if(e != OST_OK)
                    goto e3;

                qa = query_arg_new(key, arg1);
                if(qa == NULL) {
                    e = OST_MALLOC;
                    goto e4;
                }

                *target = qa;
                target = &qa->next;
            }
        }

        strbuf_free(argv, argc);
    }
    else { // if(e == 1)
        path = strdup(query);
        if(path == NULL) {
            e = OST_MALLOC;
            goto e0;
        }
    }

    *path_ret = path;
    *args_ret = root;

    return OST_OK;

e5: free(arg2);
e4: free(arg1);
e3: if(key != NULL)
        free(key);
e2: strbuf_free(argv, argc);
    ost_query_args_destroy(root);
e1: free(path);

e0: return e;
}

//
// client api
//

int ost_connect(char *host, int port, int timeout, bool secure, struct ost_limits *limits, struct ost_conn **conn_ret) {
    int e,
        fd;
    struct addrinfo hints,
                    *info,
                    *ai;
    char service[NI_MAXSERV];
    struct ost_conn *c;

    memset(&hints, 0, sizeof(struct addrinfo));

    if(port < 0)
        port = DEFAULT_PORT;

    snprintf(service, NI_MAXSERV, "%u", port);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    errno = 0;
    e = getaddrinfo(host, service, &hints, &info);
    if(e != 0) {
        e = OST_CONNECT;
        goto e0;
    }

    c = NULL;

    for(ai=info; ai != NULL; ai=ai->ai_next) {
        fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
        if(fd == -1) {
            e = OST_CONNECT;
            continue;
        }

        if(connect(fd, ai->ai_addr, ai->ai_addrlen) == -1) {
            close(fd);
            e = OST_CONNECT;
            continue;
        }

        e = ost_conn_new(fd, port, timeout, limits,
                         (struct sockaddr_storage *)ai->ai_addr, ai->ai_addrlen, &c);

        if(e != OST_OK)
            goto e1;

        break;
    }

    if(c == NULL)
        return OST_CONNECT;

    if(secure) {
        e = keyx_slave(c);
        if(e != OST_OK)
            goto e1;
    }

    if(conn_ret != NULL)
        *conn_ret = c;

e1: freeaddrinfo(info);

e0: return e;
}

int ost_queue(struct ost_conn *c, void *buf, size_t n) {
    int e;

    // forward secrecy
    if(c->secure.keyx.slave && c->secure.keyx.in_progress == 0 &&
       c->secure.key.state > 0 && c->main_out.len == 0 && c->aux_out.len == 0 &&
       conn_rsp_pending(c) == false)
    {
        e = keyx_slave(c);
        if(e != OST_OK)
            return e;
    }

    e = ost_buf_grow(&c->main_out, n);
    if(e != OST_OK)
        return e;

    return ost_buf_append(&c->main_out, buf, n);
}

int ost_flush(struct ost_conn *c) {
    int e;
    struct pollfd p;
    struct conn_worker_out *w,
                           *wn;

    e = OST_OK;

    p.fd = c->fd;
    p.events = POLLHUP|POLLOUT;

    c->flush_out = true;

    while(c->flush_out && (c->main_out.len > 0 || c->workers_out != NULL)) {
        e = poll(&p, 1, c->timeout);
        if(e == -1) {
            e = OST_POLL;
            break;
        }

        if(p.revents & POLLHUP) {
            e = OST_HANG_UP;
            break;
        }

        if(p.revents & POLLOUT) {
            workers_out_iter(c);

            e = c->iter_out_func(c);
            if(e != OST_OK)
                break;
        }
    }

    c->flush_out = false;

    return e;
}

int ost_sync(struct ost_conn *c) {
    int e;
    size_t n,i;
    struct conn_ctx *ctx;
    struct pollfd p;

    n=0;
    for(i=0; i < c->n_contexts; ++i) {
        ctx = &((struct conn_ctx *)c->contexts)[i];
        if(ctx->pending_rsp_count > 0)
            n += ctx->pending_rsp_count;
    }

    if(n == 0) {
        ost_debug("[sync] no responses pending\n");
        return OST_OK;
    }

    ost_debug("[sync] %zu responses pending\n", n);

    e = ost_flush(c);
    if(e != OST_OK)
        return e;

    c->sync_in = true;

    p.fd = c->fd;
    p.events = POLLHUP|POLLIN;

    while(c->sync_in) {
        e = poll(&p, 1, c->timeout);
        if(e == -1) {
            if(errno == EINTR)
                e = OST_INTERRUPT;
            else
                e = OST_POLL;

            fprintf(stderr, "interrupt or poll error\n");
            break;
        }
        else if(e == 0) {
            e = OST_TIMEOUT;
            break;
        }

        if(p.revents & POLLHUP) {
            e = OST_HANG_UP;
            break;
        }

        if(p.revents & POLLIN) {
            e = ost_buf_recv(&c->main_in, c, -1);
            if(e != OST_OK)
                return e;

            e = c->iter_in_func(c);
            if(e == OST_COMMIT) {
                if(--n == 0) {
                    e = OST_OK;
                    break;
                }
            }
            else if(e == OST_PARTIAL ||
                    e == OST_BAD_INPUT)
            {
                continue;
            }
            else if(e != OST_OK) {
                break;
            }
        }
    }

    c->sync_in = false;

    return e;
}

void ost_interrupt(struct ost_conn *c) {
    c->sync_in = false;
    c->flush_out = false;
}

int ost_data(struct ost_conn *c, int ctx_id, char *key, void *data, size_t size) {
    int e;
    char *cmd_fmt,
         *cmd;
    size_t cmd_size,
           cmd_len;

    cmd_fmt = "data %i \"%s\" %zu\n";

    cmd_len = snprintf(NULL, 0, cmd_fmt, ctx_id, key, size);
    cmd_size = cmd_len+1;

    cmd = alloca(cmd_size);
    snprintf(cmd, cmd_size, cmd_fmt, ctx_id, key, size);

    e = ost_queue(c, cmd, cmd_len);
    if(e != OST_OK)
        goto e1;

    e = ost_queue(c, data, size);
    if(e != OST_OK)
        goto e2;

    ost_debug("[%i: data out] key = \e[97m%s\e[0m, size = \e[97m%zu\e[0m\n", ctx_id, key, size);

    return OST_OK;

e2: ost_buf_rtrim(&c->main_out, cmd_len);
e1: free(cmd);

    return e;
}

int ost_str_data(struct ost_conn *c, int ctx_id, char *key, char *str, ssize_t len) {
    int e;
    char *cmd_fmt,
         *cmd;
    size_t size,
           cmd_size,
           cmd_len;

    if(len < 0)
        len = strlen(str);

    cmd_fmt = "data %i \"%s\" %zu\n";
    size = len+1;

    cmd_len = snprintf(NULL, 0, cmd_fmt, ctx_id, key, size);
    cmd_size = cmd_len+1;

    cmd = alloca(cmd_size);
    snprintf(cmd, cmd_size, cmd_fmt, ctx_id, key, size);

    e = ost_queue(c, cmd, cmd_len);
    if(e != OST_OK)
        goto e1;

    e = ost_queue(c, str, size);
    if(e != OST_OK)
        goto e2;

    ost_debug("[%i: data out] key = \e[97m%s\e[0m, size = \e[97m%zu\e[0m\n", ctx_id, key, size);

    return OST_OK;

e2: ost_buf_rtrim(&c->main_out, cmd_len);
e1: free(cmd);
    
    return e;
}

int ost_int_data(struct ost_conn *c, int ctx_id, char *key, long int val) {
    size_t size;
    char *data;

    size = snprintf(NULL, 0, "%li", val)+1;
    data = alloca(size);
    snprintf(data, sizeof(data), "%li", val);

    return ost_data(c, ctx_id, key, data, size);
}

int ost_uint_data(struct ost_conn *c, int ctx_id, char *key, unsigned long int val) {
    size_t size;
    char *data;

    size = snprintf(NULL, 0, "%lu", val)+1;
    data = alloca(size);
    snprintf(data, sizeof(data), "%lu", val);

    return ost_data(c, ctx_id, key, data, size);
}

int ost_float_data(struct ost_conn *c, int ctx_id, char *key, long double val) {
    size_t size;
    char *data;

    size = snprintf(NULL, 0, "%Lf", val)+1;
    data = alloca(size);
    snprintf(data, sizeof(data), "%Lf", val);

    return ost_data(c, ctx_id, key, data, size);
}

int ost_status(struct ost_conn *c, int ctx_id, unsigned int code) {
    int e;
    size_t cmd_len;
    char cmd[32];

    cmd_len = snprintf(cmd, sizeof(cmd), "status %i %u\n", ctx_id, code);
    e = ost_queue(c, cmd, cmd_len);
    ost_debug("[%i: status out] \e[97m%i\e[0m %s\n", ctx_id, code, ost_status_str[code]);

    return e;
}

int ost_commit(struct ost_conn *c, int ctx_id, char *path) {
    int e;
    struct conn_ctx *ctx;
    char *cmd_fmt,
         *cmd;
    size_t cmd_size,
           cmd_len;

    if(path != NULL) {
        cmd_fmt = "commit %i %s\n";
        cmd_len = snprintf(NULL, 0, cmd_fmt, ctx_id, path);
        cmd_size = cmd_len+1;

        cmd = malloc(cmd_size);
        if(cmd == NULL) {
            e = OST_MALLOC;
            goto r0;
        }

        snprintf(cmd, cmd_size, cmd_fmt, ctx_id, path);
        e = ost_queue(c, cmd, cmd_len);
        ost_debug("[%i: commit out] \e[97m%s\e[0m\n", ctx_id, path);

        free(cmd);

        if(e == OST_OK) {
            e = conn_ctx_ensure(c, ctx_id, &ctx);
            if(e != OST_OK)
                goto r0;

            ++ctx->pending_rsp_count;
        }
    }
    else {
        cmd_fmt = "commit %i\n";
        cmd_len = snprintf(NULL, 0, cmd_fmt, ctx_id);
        cmd_size = cmd_len+1;
        cmd = alloca(cmd_size);

        snprintf(cmd, cmd_size, "commit %i\n", ctx_id);
        e = ost_queue(c, cmd, cmd_len);
        ost_debug("[%i: commit out] \e[37mNULL\e[0m\n", ctx_id);
    }

r0: return e;
}

int ost_get_status(struct ost_conn *c, int ctx_id) {
    int e;
    struct conn_ctx *ctx;

    e = conn_ctx_get(c, ctx_id, &ctx);
    if(e != OST_OK)
        return -1;

    return ctx->status;
}

int ost_get_data(struct ost_conn *c, int ctx_id, char *key, void **data_ret, size_t *size_ret) {
    int e;
    struct conn_ctx *ctx;
    struct conn_data *d;

    e = conn_ctx_get(c, ctx_id, &ctx);
    if(e != OST_OK)
        return e;

    for(d=ctx->data; d != NULL; d=d->next) {
        if(strcmp(d->key, key) == 0) {
            *data_ret = d->buf;
            *size_ret = d->size;

            return OST_OK;
        }
    }

    return OST_ERROR;
}

int ost_get_str_data(struct ost_conn *c, int ctx_id, char *key, char **str_ret, size_t *len_ret) {
    int e;
    char *data;
    size_t size,
           len;

    e = ost_get_data(c, ctx_id, key, (void **)&data, &size);
    if(e != OST_OK)
        return e;

    len = size-1;
    if(data[len] != '\0')
        return OST_BAD_INPUT;

    if(str_ret != NULL)
        *str_ret = data;
    if(len_ret != NULL)
        *len_ret = len;

    return OST_OK;
}

int ost_get_int_data(struct ost_conn *c, int ctx_id, char *key, long int *val_ret) {
    int e;
    char *str,
         *end;
    size_t len;
    long int val;

    e = ost_get_str_data(c, ctx_id, key, &str, &len);
    if(e != OST_OK)
        return e;

    errno = 0;
    val = strtol(str, &end, 10);
    if(errno != 0 || end == str)
        return OST_BAD_INPUT;

    if(val_ret != NULL)
        *val_ret = val;

    return OST_OK;
}

int ost_get_uint_data(struct ost_conn *c, int ctx_id, char *key, unsigned long int *val_ret) {
    int e;
    char *str,
         *end;
    size_t len;
    unsigned long int val;

    e = ost_get_str_data(c, ctx_id, key, &str, &len);
    if(e != OST_OK)
        return e;

    errno = 0;
    val = strtoul(str, &end, 10);
    if(errno != 0 || end == str)
        return OST_BAD_INPUT;

    if(val_ret != NULL)
        *val_ret = val;

    return OST_OK;
}

int ost_get_float_data(struct ost_conn *c, int ctx_id, char *key, long double *val_ret) {
    int e;
    char *str,
         *end;
    size_t len;
    long double val;

    e = ost_get_str_data(c, ctx_id, key, &str, &len);
    if(e != OST_OK)
        return e;

    errno = 0;
    val = strtold(str, &end);
    if(errno != 0 || end == str)
        return OST_BAD_INPUT;

    if(val_ret != NULL)
        *val_ret = val;

    return OST_OK;
}

int ost_get_arg(struct ost_query_data *q, char *key, char **value_ret) {
    return query_arg_get(q->args, key, value_ret);
}

int ost_get_token(struct ost_query_data *q, char *key, char **value_ret) {
    return query_arg_get(q->tokens, key, value_ret);
}
