#include <ostara.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>

int print_msg(unsigned int pre_colour, char *pre, char *fmt, ...) {
    char _fmt[256];
    va_list ap;
    int n;

    snprintf(_fmt, sizeof(_fmt), "\e[%um%s\e[37m\e[0m %s", pre_colour, pre, fmt);

    va_start(ap, fmt);
    n = vfprintf(stderr, _fmt, ap);
    va_end(ap);

    return n;
}

#define print_info(...) print_msg(96, "info:", __VA_ARGS__)
#define print_warning(...) print_msg(93, "warning:", __VA_ARGS__)
#define print_error(...) print_msg(91, "error:", __VA_ARGS__)
#define print_sys_error(e) print_error("%s\n", sys_errlist[e])
#define print_nc_error(e) print_error("%s\n", nc_error_str[e])
#define print_ost_error(e) print_error("%s\n", ost_error_str[e])

int write_data(struct ost_conn *c, int ctx_id, char *data, size_t len, void *aux) {
    fwrite(data, 1, len, stdout);
    fflush(stdout);
    return OST_OK;
}

void print_body(struct ost_conn *c, int ctx_id) {
    char *body;
    size_t size;

    if(ost_get_data(c, 0, "body", (void **)&body, &size) == OST_OK) {
        fwrite(body, 1, size, stdout);
        fflush(stdout);

        if(body[size-1] != '\n') {
            fwrite("\n", 1, 1, stderr);
            fflush(stderr);
        }
    }
}

//
// main stuff
//

struct ost_conn *c;

void quit(int signal) {
    fwrite("\b\b", 2, 1, stdout);
    fflush(stdout);

    ost_interrupt(c);
}

int main(int argc, char **argv) {
    struct sigaction sa;
    int e,
        url_port,
        rsp;
    size_t i,
           ni;
    char *end,
         *host,
         *query,
         *path,
         *st,
         *rloc,
         *info;
    char cl_str[32],
         co_str[32];
    size_t cl_size,
           co_size;

    // SIGINT handler
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = quit;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);

    char *url = NULL,
         *upload_filename = NULL;
    int port = -1,
        timeout = 5000;
    bool verbose = false,
         secure = true;
    size_t content_offset = 0;
    ssize_t content_length = -1;

    for(i=1; i < argc; i=ni) {
        ni=i+1;

        if(strcmp(argv[i], "-v") == 0) {
            verbose = true;
        }
        else if(strcmp(argv[i], "-p") == 0) {
            secure = false;
        }
        else if(strcmp(argv[i], "-t") == 0) {
            if(ni >= argc) {
                print_error("no timeout specified\n");
                return 1;
            }

            errno = 0;
            timeout = strtol(argv[ni], &end, 10);
            if(errno != 0 || end == argv[ni]) {
                print_error("invalid timeout specified\n");
                return 1;
            }

            ++ni;
        }
        else if(strcmp(argv[i], "-co") == 0) {
            if(ni >= argc) {
                print_error("no content offset specified\n");
                return 1;
            }

            errno = 0;
            content_offset = strtoul(argv[ni], &end, 10);
            if(errno != 0 || end == argv[ni]) {
                print_error("invalid content offset specified\n");
                return 1;
            }

            ++ni;
        }
        else if(strcmp(argv[i], "-cl") == 0) {
            if(ni >= argc) {
                print_error("no content length specified\n");
                return 1;
            }

            errno = 0;
            content_length = strtol(argv[ni], &end, 10);
            if(errno != 0 || end == argv[ni]) {
                print_error("invalid content length specified\n");
                return 1;
            }

            ++ni;
        }
        else if(strcmp(argv[i], "-u") == 0) {
            if(ni >= argc) {
                print_error("no filename specified\n");
                return 1;
            }

            upload_filename = argv[ni];

            ++ni;
        }
        else {
            url = argv[i];
            break;
        }
    }

    if(url == NULL) {
        print_error("no url specified\n");
        return 1;
    }

    e = ost_parse_url(url, &host, &port, &query);
    if(e != OST_OK)
        goto e0;

    if(verbose) {
        print_msg(37, "*", "host = \e[97m%s\e[0m\n", host);
        print_msg(37, "*", "port = \e[97m%i\e[0m\n", port);
        print_msg(37, "*", "query = \e[97m%s\e[0m\n", query);
    }

    e = ost_connect(host, port, timeout, secure, NULL, &c);
    if(e != OST_OK)
        goto e1;

    if(content_offset > 0)
        ost_uint_data(c, 0, "content-offset", content_offset);
    if(content_length >= 0)
        ost_int_data(c, 0, "content-length", content_length);

    e = ost_commit(c, 0, query);
    if(e != OST_OK)
        goto e2;

    if(verbose)
        print_msg(37, ">", "commit \e[97m%s\e[0m\n", query);

resync:

    e = ost_sync(c);
    if(e != OST_OK)
        goto e2;

    rsp = ost_get_status(c,0);
    if(verbose)
        print_msg(37, "<", "status \e[97m%i\e[0m (%s)\n", rsp, ost_status_str[rsp]);

    if(rsp == OST_RSP_OK) {
        print_body(c,0);
    }
    else if(rsp == OST_RSP_CONTINUE) {
        if(ost_get_str_data(c, 0, "transfer-type", &st, NULL) == OST_OK) {
            if(strcmp(st, "continuous") == 0) {
                ost_conn_set_timeout(c, -1);

                e = ost_conn_set_worker_in(c, 0, "body", write_data, stdout);
                if(e != OST_OK)
                    goto e2;

                e = ost_sync(c);
                if(e != OST_OK)
                    goto e2;
            }
            else if(strcmp(st, "download") == 0) {
                e = ost_conn_set_worker_in(c, 0, "body", write_data, stdout);
                if(e != OST_OK)
                    goto e2;

                e = ost_sync(c);
                if(e != OST_OK)
                    goto e2;

                if(verbose) {
                    rsp = ost_get_status(c,0);
                    print_msg(37, "<", "status \e[97m%i\e[0m %s\n", rsp, ost_status_str[rsp]);
                }
            }
            else if(strcmp(st, "upload") == 0) {
                e = ost_conn_set_stream_out(c, 0, upload_filename, 0, -1, -1);
                if(e != OST_OK)
                    goto e2;
            }
        }
        else {
            print_error("no transfer type specified\n");
        }
    }
    else if(rsp == OST_RSP_REDIRECT) {
        if(ost_get_str_data(c, 0, "location", &rloc, NULL) == OST_OK) {
            print_body(c,0);

            ost_commit(c, 0, rloc);
            if(verbose)
                print_msg(37, ">", "commit \e[93m%s\e[0m\n", rloc);

            goto resync;
        }
        else {
            print_error("no location given for redirect\n");
            e = OST_BAD_INPUT;
            goto e2;
        }
    }
    else if(rsp == OST_RSP_ERROR) {
        print_error("server error\n");
    }

    ost_conn_destroy(c);
    if(query != NULL)
        free(query);
    if(host != NULL)
        free(host);

    return 0;

e2: ost_conn_destroy(c);
e1: if(query != NULL)
        free(query);
    if(host != NULL)
        free(host);

e0: print_error("%s\n", ost_error_str[e]);

    return 1;
}
